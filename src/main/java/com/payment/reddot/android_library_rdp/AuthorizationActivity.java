package com.payment.reddot.android_library_rdp;

/**
 * Created by Fadli on 8/15/2016.
 */

import android.app.Activity;
import android.content.Intent;
import android.net.UrlQuerySanitizer;
import android.net.http.SslError;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.webkit.JsResult;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.payment.reddot.android_library_rdp.main.Constant;
import com.payment.reddot.android_library_rdp.main.DataProcessor;
import com.payment.reddot.android_library_rdp.main.RdpProcessor;
import com.payment.reddot.android_library_rdp.main.Verificator;
import com.payment.reddot.android_library_rdp.payment.CreatePaymentResponse;
import com.payment.reddot.android_library_rdp.payment.GetPaymentResult;
import com.payment.reddot.android_library_rdp.payment.PaymentRequestV2;

import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONException;

import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.List;
import java.util.TreeMap;
import java.util.concurrent.ExecutionException;

/**
 * Created by Fadli on 8/10/2016.
 */
public class AuthorizationActivity extends Activity {
    private WebView webView;
    private TextView btnCancel, tvEnvironment;
    private HashMap<String, String> responseParameter;
    private String payment_url = "", tempSignature = "", mid = "",
            secret_key = "", tempTransactionId = "";
    private Gson gson;
    private int environment;
    private GetPaymentResult detailPayment;
    private String urlRequestPayment, jsonBeforeSignature;
    private PaymentRequestV2 request;
    private TreeMap<String, String> sortedMap;
    Verificator verificator = new Verificator();
    CreatePaymentResponse createPaymentTicketResponse = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.layout_webview);
        gson = new Gson();

        if (getIntent().getExtras().getBoolean("from_auto_intent")) {
            try {
                //getting the payment url
                initializeDataFromAutoIntent();
            } catch (Exception e) {

            }
        } else { //customizable
            try {
                initializeData();
            } catch (Exception e) {

            }
        }

        if (getIntent().getExtras().getBoolean("from_sdk")) {
            initializeViewFromSdk();
        } else {
            initializeView();
        }
    }

    private void initializeView() {
        btnCancel = (TextView) findViewById(R.id.btnExit);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        tvEnvironment = (TextView) findViewById(R.id.tvEnvironment);
        if (environment == Constant.ENVIRONMENT_SANDBOX)
            tvEnvironment.setText("Testing Environment");
        else
            tvEnvironment.setText("");

        webView = (WebView) findViewById(R.id.webView);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebChromeClient(new WebChromeClient() {
            @Override
            public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
                return super.onJsAlert(view, url, message, result);
            }
        });
        try {
            webView.loadUrl(payment_url);
        } catch (Exception e) {

        }

        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                if (error.hasError(SslError.SSL_INVALID)) {
                    handler.cancel();
                } else {
                    handler.proceed();
                }
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                RdpProcessor rdp = new RdpProcessor(environment, mid,
                        secret_key, "key");
                Intent intent = getIntent();
                generateResponse(url);

                if (tempTransactionId == null) {

                } else {
                    try {
                        detailPayment = rdp.getPaymentResult(tempTransactionId);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    if (detailPayment.getResponseCode().equals("0")) {
                        setResult(RESULT_OK, intent);
                    } else {
                        setResult(RESULT_CANCELED, intent);
                    }

                    intent.putExtra("response", "" + gson.toJson(detailPayment));
                    finish();
                }
                return true;
            }
        });
    }

    private void initializeViewFromSdk() {
        btnCancel = (TextView) findViewById(R.id.btnExit);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        tvEnvironment = (TextView) findViewById(R.id.tvEnvironment);
        if (environment == Constant.ENVIRONMENT_SANDBOX)
            tvEnvironment.setText("Testing Environment");
        else
            tvEnvironment.setText("");

        webView = (WebView) findViewById(R.id.webView);
        webView.setWebChromeClient(new WebChromeClient() {
            @Override
            public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
                return super.onJsAlert(view, url, message, result);
            }
        });
        try {
            webView.loadUrl(payment_url);
        } catch (Exception e) {

        }
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getCertificate();
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                if (error.hasError(SslError.SSL_INVALID)) {
                    handler.cancel();
                } else {
                    handler.proceed();
                }
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);

                RdpProcessor rdp = new RdpProcessor(environment, mid,
                        secret_key, "key");
                Intent intent = getIntent();
                generateResponse(url);

                if (tempTransactionId == null) {

                } else {
                    try {
                        detailPayment = rdp.getPaymentResult(tempTransactionId);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    if (detailPayment.getResponseCode().equals("0")) {
                        setResult(RESULT_OK, intent);
                    } else {
                        setResult(RESULT_CANCELED, intent);
                    }
                    //returning response to merchant
                    intent.putExtra("response", "" + gson.toJson(detailPayment));
                    finish();
                }
                return true;
            }
        });
    }

    private void initializeData() {
        payment_url = getIntent().getExtras().getString("payment_url");
        mid = getIntent().getExtras().getString("mid");
        secret_key = getIntent().getExtras().getString("secret_key");
        environment = getIntent().getExtras().getInt("environment");
    }

    private void initializeDataFromAutoIntent() {
        Gson gson = new Gson();
        String response = "";

        urlRequestPayment = getIntent().getExtras().getString("urlRequestPayment");
        jsonBeforeSignature = getIntent().getExtras().getString("jsonBeforeSignature");
        request = gson.fromJson(jsonBeforeSignature, PaymentRequestV2.class);

        mid = getIntent().getExtras().getString("mid");
        secret_key = getIntent().getExtras().getString("secret_key");
        environment = getIntent().getExtras().getInt("environment");

        try {
            sortedMap = new ObjectMapper().readValue(jsonBeforeSignature, TreeMap.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        StringBuilder calculateString = new StringBuilder();
        calculateString.append(mid);
        calculateString.append(request.getOrderId());
        calculateString.append(request.getPaymentType());
        calculateString.append(request.getAmount());
        calculateString.append(request.getCcy());
        calculateString.append(request.getPayerId() == null ? "" : request.getPayerId());
        calculateString.append(secret_key);

        JsonElement jsonElement = gson.toJsonTree(request);
        if (jsonElement.getAsJsonObject().get("payment_channel") == null || jsonElement.getAsJsonObject().get("payment_channel").toString().equals("0")) {
            jsonElement.getAsJsonObject().remove("payment_channel");
        }
        jsonElement.getAsJsonObject().addProperty("signature", verificator.generateHash(calculateString.toString()));
        String jsonAfterSignature = gson.toJson(jsonElement);

        DataProcessor processData = new DataProcessor(jsonAfterSignature, urlRequestPayment);
        try {
            response = processData.execute().get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        createPaymentTicketResponse = new CreatePaymentResponse(response, secret_key);

        if (createPaymentTicketResponse.getResponseCode() == null) {
            Intent intent = getIntent();
            setResult(RESULT_CANCELED, intent);
            intent.putExtra("response", "Cannot get response from Web Service. Try to check your internet connection or contact our support.");
            finish();
        } else {
            if (createPaymentTicketResponse.getResponseCode().equals("0")) {
                payment_url = createPaymentTicketResponse.getPaymentUrl().toString();
            } else {
                Intent intent = getIntent();
                setResult(RESULT_CANCELED, intent);
                intent.putExtra("response", "" + gson.toJson(createPaymentTicketResponse));
                finish();
            }
        }
    }

    private void generateResponse(String fromUrl) {
        UrlQuerySanitizer sanitizer = new UrlQuerySanitizer();
        sanitizer.setAllowUnregisteredParamaters(true);
        sanitizer.setUnregisteredParameterValueSanitizer(UrlQuerySanitizer.getAllButNulLegal());
        sanitizer.parseUrl(fromUrl);
        List<UrlQuerySanitizer.ParameterValuePair> temp = sanitizer.getParameterList();
        responseParameter = new HashMap<String, String>();
        for (UrlQuerySanitizer.ParameterValuePair test : temp) {
            if (test.mParameter.equals("signature")) {
                tempSignature = test.mValue;
            } else {
                responseParameter.put(test.mParameter, test.mValue);
            }
        }
        tempTransactionId = responseParameter.get("transaction_id");
    }

    public static String calculateMd5(String... data) {
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("MD5");
            for (final String s : data) {
                md.update(s.getBytes());
            }
        } catch (NoSuchAlgorithmException ex) {
            throw new RuntimeException("MD5 Cryptography Not Supported");
        }
        final BigInteger bigInt = new BigInteger(1, md.digest());
        return String.format("%032x", bigInt);
    }
}