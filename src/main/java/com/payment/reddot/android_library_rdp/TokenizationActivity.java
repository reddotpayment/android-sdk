package com.payment.reddot.android_library_rdp;

import android.app.Activity;
import android.content.Intent;
import android.net.UrlQuerySanitizer;
import android.net.http.SslError;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.webkit.JsResult;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.google.gson.Gson;
import com.payment.reddot.android_library_rdp.main.Constant;
import com.payment.reddot.android_library_rdp.main.RdpProcessor;
import com.payment.reddot.android_library_rdp.tokenization.CreateTokenResult;

import org.json.JSONException;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Fadli on 8/10/2016.
 */
public class TokenizationActivity extends Activity {
    private WebView webView;
    private TextView btnCancel, tvEnvironment;
    private HashMap<String, String> responseParameter;
    private String payment_url = "", mid = "",
            secret_key = "", tempTransactionId = "";
    private Gson gson;
    private int environment;
    private CreateTokenResult detailToken;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.layout_webview);
        gson = new Gson();

        try {
            initializeData();
        } catch (Exception e) {

        }

        if (getIntent().getExtras().getBoolean("from_sdk")) {
            initializeViewFromSdk();
        } else {
            initializeView();
        }
    }

    private void initializeView() {
        btnCancel = (TextView) findViewById(R.id.btnExit);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        tvEnvironment = (TextView) findViewById(R.id.tvEnvironment);
        if (environment == Constant.ENVIRONMENT_SANDBOX)
            tvEnvironment.setText("Testing Environment");
        else
            tvEnvironment.setText("");

        webView = (WebView) findViewById(R.id.webView);
        webView.setWebChromeClient(new WebChromeClient() {
            @Override
            public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
                return super.onJsAlert(view, url, message, result);
            }
        });

        try {
            webView.loadUrl(payment_url);
        } catch (Exception e) {

        }
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                if (error.hasError(SslError.SSL_INVALID)) {
                    handler.cancel();
                } else {
                    handler.proceed();
                }
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                RdpProcessor rdp = new RdpProcessor(environment, mid,
                        secret_key, "key");
                Intent intent = getIntent();
                generateResponse(url);
                try {
                    detailToken = rdp.getTokenResult(tempTransactionId);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (detailToken.getResponseCode().equals("0")) {
                    setResult(RESULT_OK, intent);
                } else {
                    setResult(RESULT_CANCELED, intent);
                }

                intent.putExtra("response", "" + gson.toJson(detailToken));
                finish();

                return true;
            }
        });
    }

    private void initializeViewFromSdk() {
        btnCancel = (TextView) findViewById(R.id.btnExit);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        tvEnvironment = (TextView) findViewById(R.id.tvEnvironment);
        if (environment == Constant.ENVIRONMENT_SANDBOX)
            tvEnvironment.setText("Testing Environment");
        else
            tvEnvironment.setText("");

        webView = (WebView) findViewById(R.id.webView);
        webView.setWebChromeClient(new WebChromeClient() {
            @Override
            public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
                return super.onJsAlert(view, url, message, result);
            }
        });
        try {
            webView.loadUrl(payment_url);
        } catch (Exception e) {

        }
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                if (error.hasError(SslError.SSL_INVALID)) {
                    handler.cancel();
                } else {
                    handler.proceed();
                }
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                RdpProcessor rdp = new RdpProcessor(environment, mid,
                        secret_key, "key");
                Intent intent = getIntent();
                generateResponse(url);

                try {
                    detailToken = rdp.getTokenResult(tempTransactionId);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (detailToken.getResponseCode().equals("0")) {
                    setResult(RESULT_OK, intent);
                } else {
                    setResult(RESULT_CANCELED, intent);
                }

                intent.putExtra("response", "" + gson.toJson(detailToken));
                finish();

                return true;
            }
        });
    }

    private void initializeData() {
        payment_url = getIntent().getExtras().getString("payment_url");
        mid = getIntent().getExtras().getString("mid");
        secret_key = getIntent().getExtras().getString("secret_key");
        environment = getIntent().getExtras().getInt("environment");
    }

    private void generateResponse(String fromUrl) {
        UrlQuerySanitizer sanitizer = new UrlQuerySanitizer();
        sanitizer.setAllowUnregisteredParamaters(true);
        sanitizer.setUnregisteredParameterValueSanitizer(UrlQuerySanitizer.getAllButNulLegal());
        sanitizer.parseUrl(fromUrl);
        List<UrlQuerySanitizer.ParameterValuePair> temp = sanitizer.getParameterList();
        responseParameter = new HashMap<String, String>();

        for (UrlQuerySanitizer.ParameterValuePair test : temp) {
            if (test.mParameter.equals("signature")) {
                //do nothing
            } else {
                responseParameter.put(test.mParameter, test.mValue);
            }
        }
        tempTransactionId = responseParameter.get("transaction_id");
    }

    public static String calculateMd5(String... data) {
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("MD5");
            for (final String s : data) {
                md.update(s.getBytes());
            }
        } catch (NoSuchAlgorithmException ex) {
            throw new RuntimeException("MD5 Cryptography Not Supported");
        }
        final BigInteger bigInt = new BigInteger(1, md.digest());
        return String.format("%032x", bigInt);
    }
}