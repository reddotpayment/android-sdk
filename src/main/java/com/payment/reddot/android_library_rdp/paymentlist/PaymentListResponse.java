/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.payment.reddot.android_library_rdp.paymentlist;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.payment.reddot.android_library_rdp.main.Verificator;

import java.lang.reflect.Type;
import java.util.Map;
import java.util.TreeMap;

/**
 *
 * @author Red Dot Payment
 */
public class PaymentListResponse {

    private String mid;
    private String random_seed;
    private String response_code;
    private String response_msg;
    private PaymentListDetail[] payment_channel_list;

    public PaymentListResponse(String response, String secret_key) {
        String tempFirstSubstring = "";
        String tempFirstReverse = "";
        String tempSecondSubstring = "";
        String tempSecondReverse = "";
        String responseWithoutList = "";
        String paymentListNyah = "";

        try {
            tempFirstSubstring = response.substring(response.indexOf('"' + "payment_channel_list" + '"'), response.length());
        } catch (Exception e) {

        }
        try {
            tempFirstReverse = new StringBuilder(tempFirstSubstring).reverse().toString();
        } catch (Exception e) {

        }
        try {
            tempSecondSubstring = tempFirstReverse.substring(tempFirstReverse.indexOf("]"), tempFirstReverse.length());
        } catch (Exception e) {

        }

        try {
            tempSecondReverse = new StringBuilder(tempSecondSubstring).reverse().toString();
        } catch (Exception e) {

        }

        try {
            paymentListNyah = tempSecondReverse.substring(tempSecondReverse.indexOf("["), tempSecondReverse.length());
        } catch (Exception e) {

        }

        try {
            responseWithoutList = response.replace("," + tempSecondReverse, "");
        } catch (Exception e) {
            responseWithoutList = response;
        }

        StringBuilder calculateString = new StringBuilder();
        Verificator verificator = new Verificator();
        Gson gson = new Gson();
        Type stringStringMap = new TypeToken<Map<String, String>>() {
        }.getType();

        JsonElement element = new JsonParser().parse("{" + tempSecondReverse + "}");
        JsonObject objList = element.getAsJsonObject();

        payment_channel_list = gson.fromJson(paymentListNyah, PaymentListDetail[].class);
        StringBuilder tempList2 = new StringBuilder();

        try {
            for (int i = 0; i < payment_channel_list.length; i++) {
                Map<String, String> mapListDetail = gson.fromJson(gson.toJson(payment_channel_list[i]), stringStringMap);
                TreeMap<String, String> sortedParameterListDetail = new TreeMap<>(mapListDetail);

                for (Map.Entry<String, String> treeMap : sortedParameterListDetail.entrySet()) {
                    tempList2.append(treeMap.getValue());
                }
            }
        } catch (Exception e) {

        }

        StringBuilder tempList = new StringBuilder();
        StringBuilder calculateStringList = new StringBuilder();
        Map<String, String> map;
        try{
            map = gson.fromJson(responseWithoutList, stringStringMap);
        }catch(Exception e){
            map = gson.fromJson(response, stringStringMap);
        }

        if (!paymentListNyah.equals("")) {
            map.put("payment_channel_list", paymentListNyah);
        }
        TreeMap<String, String> sortedParameter = new TreeMap<>(map);
        for (Map.Entry<String, String> treeMap : sortedParameter.entrySet()) {
            if (treeMap.getKey().equals("signature")) {
            } else if (treeMap.getKey().equals("payment_channel_list")) {
                calculateString.append(tempList2);
            } else {
                calculateString.append(treeMap.getValue());
            }
        }

        calculateString.append(secret_key);
        try {
            if (map.get("signature").toString().equals(verificator.generateHash(calculateString.toString()))) {
                try {
                    this.mid = map.get("mid").toString();
                } catch (Exception e) {
                }
                try {
                    this.random_seed = map.get("random_seed").toString();
                } catch (Exception e) {
                }
                try {
                    this.response_code = map.get("response_code").toString();
                } catch (Exception e) {
                }
                try {
                    this.response_msg = map.get("response_msg").toString();
                } catch (Exception e) {
                }
            } else {
            }
        } catch (Exception ex) {
            try {
                this.mid = map.get("mid").toString();
            } catch (Exception e) {
            }
            try {
                this.random_seed = map.get("random_seed").toString();
            } catch (Exception e) {
            }
            try {
                this.response_code = map.get("response_code").toString();
            } catch (Exception e) {
            }
            try {
                this.response_msg = map.get("response_msg").toString();
            } catch (Exception e) {
            }
        }
    }

    public String getMid() {
        return mid;
    }

    public String getRandomSeed() {
        return random_seed;
    }

    public String getResponseCode() {
        return response_code;
    }

    public String getResponseMsg() {
        return response_msg;
    }
    
    public PaymentListDetail[] getPaymentList() {
        return payment_channel_list;
    }

}
