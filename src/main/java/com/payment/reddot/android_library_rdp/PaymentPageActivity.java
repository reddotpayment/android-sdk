package com.payment.reddot.android_library_rdp;

/**
 * Created by Fadli on 8/15/2016.
 */

import android.app.Activity;
import android.content.Intent;
import android.net.UrlQuerySanitizer;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.JsResult;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.payment.reddot.android_library_rdp.main.Constant;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.List;
import java.util.TreeMap;

import io.card.payment.CardIOActivity;
import io.card.payment.CreditCard;

/**
 * Created by Fadli on 8/10/2016.
 */
public class PaymentPageActivity extends Activity {
    private String urlRequest = "", secretKey = "", calculateString, calculateResponse, tempSignature = "";
    private int environment;
    private WebView webView;
    private TextView btnCancel, tvEnvironment;
    private StringBuilder postData = new StringBuilder();
    private StringBuilder tempCalculateString = new StringBuilder();
    private StringBuilder replyData = new StringBuilder();
    private HashMap<String, String> getParameter, responseParameter;
    private TreeMap<String, String> sortedParameter, sortedParameterResponse;
    private boolean scanMode = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.layout_webview);
        try {
            scanMode = getIntent().getExtras().getBoolean("scan_mode");
        } catch (Exception e) {

        }

        if (scanMode) {
            initializeViewScanMode();

            Intent scanIntent = new Intent(this, CardIOActivity.class);

            // customize these values to suit your needs.
            scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_EXPIRY, true);
            scanIntent.putExtra(CardIOActivity.EXTRA_SCAN_EXPIRY, true);
            scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_CVV, true);
            scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_CARDHOLDER_NAME, true);

            // MY_SCAN_REQUEST_CODE is arbitrary and is only used within this activity.
            startActivityForResult(scanIntent, Constant.CARD_IO_REQUEST_CODE);
        } else {
            try {
                initializeData();
            } catch (Exception e) {

            }
            initializeView();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Constant.CARD_IO_REQUEST_CODE) {
            String resultDisplayStr;
            if (data != null && data.hasExtra(CardIOActivity.EXTRA_SCAN_RESULT)) {
                CreditCard scanResult = data.getParcelableExtra(CardIOActivity.EXTRA_SCAN_RESULT);

                resultDisplayStr = "Card Number: " + scanResult.getRedactedCardNumber() + "\n";

                String expiryMonth = "", expiryYear = "";
                if (scanResult.expiryMonth < 10) {
                    expiryMonth = "0" + scanResult.expiryMonth;
                } else {
                    expiryMonth = "" + scanResult.expiryMonth;
                }
                expiryYear = ("" + scanResult.expiryYear).substring(2);

                secretKey = getIntent().getExtras().getString("secret_key");
                environment = getIntent().getExtras().getInt("environment");
                getParameter = (HashMap<String, String>) getIntent().getExtras().getSerializable("parameters");
                getParameter.put("card_number", (scanResult.getFormattedCardNumber()).replace(" ", ""));
                getParameter.put("cvv2", "" + scanResult.cvv);
                try {
                    getParameter.put("first_name", "" + scanResult.cardholderName.substring(0,
                            scanResult.cardholderName.indexOf(" ")));
                    int iSub = scanResult.cardholderName.indexOf(" ") + 1;
                    String last_name = scanResult.cardholderName.substring(iSub);
                    getParameter.put("last_name", "" + last_name);
                } catch (Exception e) {
                    getParameter.put("first_name", "" + scanResult.cardholderName);
                    getParameter.put("last_name", "" + scanResult.cardholderName);
                }
                getParameter.put("card_number", (scanResult.getFormattedCardNumber()).replace(" ", ""));
                getParameter.put("expiry_date", "" + expiryMonth + expiryYear);
                sortedParameter = new TreeMap<>(getParameter);
                postData = new StringBuilder();
                tempCalculateString = new StringBuilder();
                for (TreeMap.Entry<String, String> treeMap : sortedParameter.entrySet()) {
                    tempCalculateString.append(treeMap.getKey() + "=" + treeMap.getValue() + "&");
                    try {
                        postData.append(treeMap.getKey() + "=" + URLEncoder.encode(treeMap.getValue(), "UTF-8") + "&");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }
                calculateString = tempCalculateString.toString() + "secret_key=" + secretKey;

                initializeView();

            } else {
                //back button handler
                finish();
            }
            // do something with resultDisplayStr, maybe display it in a textView
            // resultTextView.setText(resultDisplayStr);
        }
        // else handle other activity results
    }

    private void initializeViewScanMode() {
        btnCancel = (TextView) findViewById(R.id.btnExit);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        environment = getIntent().getExtras().getInt("environment");
        tvEnvironment = (TextView) findViewById(R.id.tvEnvironment);
        if (environment == Constant.ENVIRONMENT_SANDBOX) {
            tvEnvironment.setText("Testing Environment");
            urlRequest = "http://test.reddotpayment.com/merchant/cgi-bin-live?";
        } else if (environment == Constant.ENVIRONMENT_LIVE) {
            tvEnvironment.setText("");
            urlRequest = "https://connect.reddotpayment.com/merchant/cgi-bin-live?";
        }
    }

    private void initializeView() {
        btnCancel = (TextView) findViewById(R.id.btnExit);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        tvEnvironment = (TextView) findViewById(R.id.tvEnvironment);
        if (environment == Constant.ENVIRONMENT_SANDBOX) {
            tvEnvironment.setText("Testing Environment");
            //urlRequest = "http://test.reddotpayment.com/merchant/cgi-bin-live?";
            urlRequest = "http://test.reddotpayment.com/merchant/cgi-bin-live?";
        } else if (environment == Constant.ENVIRONMENT_LIVE) {
            tvEnvironment.setText("");
            urlRequest = "https://connect.reddotpayment.com/merchant/cgi-bin-live?";
        }

        webView = (WebView) findViewById(R.id.webView);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            WebView.setWebContentsDebuggingEnabled(true);
        }
        webView.setWebChromeClient(new WebChromeClient() {
            @Override
            public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
                return super.onJsAlert(view, url, message, result);
            }
        });

        postData.append("&signature=" + calculateMd5(calculateString));
        String finalPost = postData.toString();

        try {
            final String s = new String(finalPost.getBytes(), "UTF-8");
            webView.postUrl(urlRequest, finalPost.getBytes());
        } catch (UnsupportedEncodingException e) {

        }

        CookieSyncManager cookieSyncManager = CookieSyncManager.createInstance(webView.getContext());
        CookieManager cookieManager = CookieManager.getInstance();
        cookieManager.setAcceptCookie(true);
        cookieManager.removeSessionCookie();
        cookieManager.setCookie("" + urlRequest, "" + finalPost);
        cookieSyncManager.sync();

        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                if (error.hasError(SslError.SSL_INVALID)) {
                    handler.cancel();
                } else {
                    handler.proceed();
                }
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                if (url.toLowerCase().contains("result=paid")) {
                    generateResponse(url, RESULT_OK);
                    finish();
                } else if (url.toLowerCase().contains("result=rejected")) {
                    generateResponse(url, RESULT_CANCELED);
                    finish();
                }
                return true;
            }
        });
    }

    private void initializeData() {
        secretKey = getIntent().getExtras().getString("secret_key");
        environment = getIntent().getExtras().getInt("environment");
        getParameter = (HashMap<String, String>) getIntent().getExtras().getSerializable("parameters");
        sortedParameter = new TreeMap<>(getParameter);
        postData = new StringBuilder();
        tempCalculateString = new StringBuilder();
        for (TreeMap.Entry<String, String> treeMap : sortedParameter.entrySet()) {
            tempCalculateString.append(treeMap.getKey() + "=" + treeMap.getValue() + "&");
            try {
                postData.append(treeMap.getKey() + "=" + URLEncoder.encode(treeMap.getValue(), "UTF-8") + "&");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        calculateString = tempCalculateString.toString() + "secret_key=" + secretKey;
    }

    public static String calculateMd5(String... data) {
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("MD5");
            for (final String s : data) {
                md.update(s.getBytes());
            }
        } catch (NoSuchAlgorithmException ex) {
            throw new RuntimeException("MD5 Cryptography Not Supported");
        }
        final BigInteger bigInt = new BigInteger(1, md.digest());
        return String.format("%032x", bigInt);
    }

    private void generateResponse(String fromUrl, int responseResult) {
        Intent intent = getIntent();
        setResult(responseResult, intent);

        UrlQuerySanitizer sanitizer = new UrlQuerySanitizer();
        sanitizer.setAllowUnregisteredParamaters(true);
        sanitizer.setUnregisteredParameterValueSanitizer(UrlQuerySanitizer.getAllButNulLegal());
        sanitizer.parseUrl(fromUrl);
        List<UrlQuerySanitizer.ParameterValuePair> temp = sanitizer.getParameterList();
        responseParameter = new HashMap<String, String>();

        for (UrlQuerySanitizer.ParameterValuePair test : temp) {
            if (test.mParameter.equals("signature")) {
                tempSignature = test.mValue;
            } else {
                responseParameter.put(test.mParameter, test.mValue);
            }
        }
        sortedParameterResponse = new TreeMap<String, String>(responseParameter);
        replyData = new StringBuilder();

        for (TreeMap.Entry<String, String> treeMap : sortedParameterResponse.entrySet()) {
            replyData.append(treeMap.getKey() + "=" + treeMap.getValue() + "&");
        }
        replyData.setLength(replyData.length() - 1);
        calculateResponse = replyData.toString() + "&secret_key=" + secretKey;

        if (tempSignature.equals(calculateMd5(calculateResponse)))
            intent.putExtra("response", responseParameter);
    }
}