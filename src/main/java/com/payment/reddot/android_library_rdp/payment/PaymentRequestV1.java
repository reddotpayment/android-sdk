package com.payment.reddot.android_library_rdp.payment;

import java.net.URL;

/**
 * Created by Fadli on 4/11/2017.
 */
public class PaymentRequestV1 {
    private String first_name;
    private String last_name;
    private String amount;
    private String email;
    private String order_number;
    private String currency_code;
    private String merchant_reference;
    private URL notify_url;
    private URL return_url;
    private String transaction_type;
    private String paymentpage_style;
    private String promo_code;
    private String address_line1;
    private String address_line2;
    private String address_city;
    private String address_portal_code;
    private String address_state;
    private String address_country;
    private String merchant_data1;
    private String merchant_data2;
    private String merchant_data3;
    private String merchant_data4;
    private String merchant_data5;
    private String token_id;
    private String order_timeout;

    public PaymentRequestV1() {

    }

    public String getPaymentpage_style() {
        return paymentpage_style;
    }

    public void setPaymentpage_style(String paymentpage_style) {
        this.paymentpage_style = paymentpage_style;
    }

    public String getPromo_code() {
        return promo_code;
    }

    public void setPromo_code(String promo_code) {
        this.promo_code = promo_code;
    }

    public String getAddress_line1() {
        return address_line1;
    }

    public void setAddress_line1(String address_line1) {
        this.address_line1 = address_line1;
    }

    public String getAddress_line2() {
        return address_line2;
    }

    public void setAddress_line2(String address_line2) {
        this.address_line2 = address_line2;
    }

    public String getAddress_city() {
        return address_city;
    }

    public void setAddress_city(String address_city) {
        this.address_city = address_city;
    }

    public String getAddress_portal_code() {
        return address_portal_code;
    }

    public void setAddress_portal_code(String address_portal_code) {
        this.address_portal_code = address_portal_code;
    }

    public String getAddress_state() {
        return address_state;
    }

    public void setAddress_state(String address_state) {
        this.address_state = address_state;
    }

    public String getAddress_country() {
        return address_country;
    }

    public void setAddress_country(String address_country) {
        this.address_country = address_country;
    }

    public String getMerchant_data1() {
        return merchant_data1;
    }

    public void setMerchant_data1(String merchant_data1) {
        this.merchant_data1 = merchant_data1;
    }

    public String getMerchant_data2() {
        return merchant_data2;
    }

    public void setMerchant_data2(String merchant_data2) {
        this.merchant_data2 = merchant_data2;
    }

    public String getMerchant_data3() {
        return merchant_data3;
    }

    public void setMerchant_data3(String merchant_data3) {
        this.merchant_data3 = merchant_data3;
    }

    public String getMerchant_data4() {
        return merchant_data4;
    }

    public void setMerchant_data4(String merchant_data4) {
        this.merchant_data4 = merchant_data4;
    }

    public String getMerchant_data5() {
        return merchant_data5;
    }

    public void setMerchant_data5(String merchant_data5) {
        this.merchant_data5 = merchant_data5;
    }

    public String getToken_id() {
        return token_id;
    }

    public void setToken_id(String token_id) {
        this.token_id = token_id;
    }

    public String getOrder_timeout() {
        return order_timeout;
    }

    public void setOrder_timeout(String order_timeout) {
        this.order_timeout = order_timeout;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getOrder_number() {
        return order_number;
    }

    public void setOrder_number(String order_number) {
        this.order_number = order_number;
    }

    public String getCurrency_code() {
        return currency_code;
    }

    public void setCurrency_code(String currency_code) {
        this.currency_code = currency_code;
    }

    public String getMerchant_reference() {
        return merchant_reference;
    }

    public void setMerchant_reference(String merchant_reference) {
        this.merchant_reference = merchant_reference;
    }

    public URL getNotify_url() {
        return notify_url;
    }

    public void setNotify_url(URL notify_url) {
        this.notify_url = notify_url;
    }

    public URL getReturn_url() {
        return return_url;
    }

    public void setReturn_url(URL return_url) {
        this.return_url = return_url;
    }

    public String getTransaction_type() {
        return transaction_type;
    }

    public void setTransaction_type(String transaction_type) {
        this.transaction_type = transaction_type;
    }
}
