/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.payment.reddot.android_library_rdp.payment;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.payment.reddot.android_library_rdp.main.Verificator;

import java.lang.reflect.Type;
import java.net.URI;
import java.net.URL;
import java.util.Map;
import java.util.TreeMap;

/**
 *
 * @author Red Dot Payment
 */
public class PaymentRequestV2 {

    private String mid;
    private String payment_type;
    private int payment_channel;
    private String signature;
    private String payer_id;
    private Integer multiple_method_page;
    private String api_mode;
    private String ccy;
    private String order_id;
    private String payer_email;
    private String amount;
    private String locale;
    private String merchant_reference;
    private URL back_url;
    private URL redirect_url;
    private URL notify_url;
    private String store_code;
    private String bill_to_forename;
    private String bill_to_surname;
    private String bill_to_address_city;
    private String bill_to_address_line1;
    private String bill_to_address_line2;
    private String bill_to_address_country;
    private String bill_to_address_state;
    private String bill_to_address_postal_code;
    private String bill_to_phone;
    private String ship_to_forename;
    private String ship_to_surname;
    private String ship_to_address_city;
    private String ship_to_address_line1;
    private String ship_to_address_line2;
    private String ship_to_address_country;
    private String ship_to_address_state;
    private String ship_to_address_postal_code;
    private String ship_to_phone;
    private boolean scan_mode;
    private URI caller_uri;
    
    public PaymentRequestV2(/*String response, String secret_key*/) {

        /*StringBuilder calculateString = new StringBuilder();
        Verificator verificator = new Verificator();
        Gson gson = new Gson();
        Type stringStringMap = new TypeToken<Map<String, String>>() {
        }.getType();
        Map<String, String> map = gson.fromJson(response, stringStringMap);
        TreeMap<String, String> sortedParameter = new TreeMap<>(map);
        for (Map.Entry<String, String> treeMap : sortedParameter.entrySet()) {
            if (treeMap.getKey().equals("signature")) {
            } else {
                calculateString.append(treeMap.getValue());
            }
        }
        calculateString.append(secret_key);
        try {
            if (map.get("signature").toString().equals(verificator.generateHash(calculateString.toString()))) {
                try {
                    this.mid = map.get("mid").toString();
                } catch (Exception e) {
                }
                try {
                    this.payer_email = map.get("payer_email").toString();
                } catch (Exception e) {
                }
                try {
                    this.payer_id = map.get("payer_id").toString();
                } catch (Exception e) {
                }
                try {
                    this.signature = map.get("signature").toString();
                } catch (Exception e) {
                }
            }
        } catch (Exception ex) {
            try {
                this.mid = map.get("mid").toString();
            } catch (Exception e) {
            }
            try {
                this.order_id = map.get("order_id").toString();
            } catch (Exception e) {
            }
            try {
                this.ccy = map.get("ccy").toString();
            } catch (Exception e) {
            }
            try {
                this.payer_email = map.get("payer_email").toString();
            } catch (Exception e) {
            }
            try {
                this.payer_id = map.get("payer_id").toString();
            } catch (Exception e) {
            }
            try {
                this.signature = map.get("signature").toString();
            } catch (Exception e) {
            }
        }*/

    }

    /**
     * [CONDITIONAL] [BOOLEAN] Setup your card number from Card IO Scanner.
     * @param scan_mode <br />
     */
    public void setScanMode(boolean scan_mode) {
        this.scan_mode = scan_mode;
    }

    public boolean getScanMode() {
        return scan_mode;
    }
    
    /**
     * [CONDITIONAL] [VARCHAR] Setup your store_code. Max length 20. 
     * Alphanumeric, underscore, dash. (Enforcement by Account Setup)
     * @param store_code <br />
     */
    public void setStoreCode(String store_code) {
        this.store_code = store_code;
    }
    
    public String getStoreCode() {
        return store_code;
    }
    
    /**
     * Setup your Caller Application URI <br />
     * This is especially important for Mobile payment which payment mode
     * involving invocation of another Application.<br /> <br />
     * @param caller_uri <br />
     * For Web application : your base URL, e.g. www.reddotpayment.com <br />
     * For Mobile application : your app URI, 
     * e.g. rdp://com.reddotpayment.pay/?txnrefId=1234567 <br />
     */
    public void setCallerApplicationURI(URI caller_uri) {
        this.caller_uri = caller_uri;
    }
    
    public URI getCallerApplicationURI() {
        return caller_uri;
    }

    public String getPaymentType() {
        return payment_type;
    }

    /**
     * 
     * @param payment_type 
     * [MANDATORY] Defining the function to be served.
     * S : Sale Transaction<br>
     * A : (Pre) Authorization<br>
     */
    public void setPaymentType(String payment_type) {
        this.payment_type = payment_type;
    }

    public int getPaymentChannel() {
        return payment_channel;
    }

    /**
     * 
     * @param payment_channel 
     * [OPTIONAL] The payment channel that used for the transaction.
     * Please refer to class PaymentChannel (Payment Channel List).
     */
    public void setPaymentChannel(int payment_channel) {
        this.payment_channel = payment_channel;
    }

    public String getPayerId() {
        return payer_id;
    }

    /**
     * 
     * @param payer_id 
     * [CONDITIONAL] Merchant defined payer ID or customer ID. 
     * Used to identify a unique merchant's customer.
     */
    public void setPayerId(String payer_id) {
        this.payer_id = payer_id;
    }

    public Integer getMultipleMethodPage() {
        return multiple_method_page;
    }

    /**
     * 
     * @param multiple_method_page [INT] 
     * [OPTIONAL] By default it has the value of 1, which means using 
     * the multiple mode page for cardholder to see the payment-summary 
     * and choose their payment method (if Merchant use multiple payment 
     * method with RDP Gateway)
     * [POSSIBLE VALUE]
     * 1: use multiple mode page, where cardholder can choose the payment-page<br>
     * 0: directly request to acquiring partner page for those acquirer 
     * which host the payment-page (e.g. eNETS, UPOP - Union Pay Online Payment)
     * If PaymentChannel is being set, it will override the meaning of this
     * configuration.
     */
    public void setMultipleMethodPage(Integer multiple_method_page) {
        this.multiple_method_page = multiple_method_page;
    }
    
    public String getLocale() {
        return locale;
    }

    /**
     * 
     * @param locale 
     * [OPTIONAL] At the moment we support only two language. We are looking 
     * forward to support more languages.
     * [POSSIBLE VALUE]
     * en : English<br>
     * id : Bahasa<br>
     */
    public void setLocale(String locale) {
        this.locale = locale;
    }

    public String getMerchantReference() {
        return merchant_reference;
    }

    /**
     * 
     * @param merchantReference 
     * [OPTIONAL] Any kind of extra information for merchant to relate
     * with this transaction.
     */
    public void setMerchantReference(String merchantReference) {
        this.merchant_reference = merchantReference;
    }

    public String getAmount() {
        return amount;
    }

    /**
     * 
     * @param amount 
     * [MANDATORY] e.g 1200.07 or 1200.
     */
    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getMid() {
        return mid;
    }

    /**
     * 
     * @param mid 
     * [MANDATORY] The merchant id given by RDP when setting up an account.
     */
    public void setMid(String mid) {
        this.mid = mid;
    }

    public String getApiMode() {
        return api_mode;
    }

    public void setApiMode(String apiMode) {
        this.api_mode = apiMode;
    }

    public String getCcy() {
        return ccy;
    }

    /**
     * 
     * @param ccy
     * [MANDATORY] ISO-4217 Alphabetical Currency Code format. e.g. IDR, USD, SGD
     */
    public void setCcy(String ccy) {
        this.ccy = ccy;
    }

    public String getOrderId() {
        return order_id;
    }

    /**
     * 
     * @param orderId
     * [MANDATORY] Merchant defined order-id for the transaction. 
     * Used for identifying the transaction request. Suggested to be of
     * unique values. Merchant can request enforcement of unique 
     * order_id from RDP (where repeated order_id is to be rejected.)
     */
    public void setOrderId(String orderId) {
        this.order_id = orderId;
    }

    public String getPayerEmail() {
        return payer_email;
    }

    /**
     * 
     * @param payerEmail 
     * [CONDITIONAL] Email address of payer or customer. Use this field to 
     * inform the email of customer. Quite useful for reviewing transactions in 
     * our Instanpanel Backend, or to contribute in transaction identification
     * for Fraud Detection System (FDS).
     */
    public void setPayerEmail(String payerEmail) {
        this.payer_email = payerEmail;
    }

    public URL getBackUrl() {
        return back_url;
    }

    /**
     * 
     * @param backUrl
     * [MANDATORY] Merchant's site URL where customer is to be redirected 
     * when they chose to press "back" button on RDP's payment page
     */
    public void setBackUrl(URL backUrl) {
        this.back_url = backUrl;
    }

    public URL getRedirectUrl() {
        return redirect_url;
    }

    /**
     * 
     * @param redirectUrl
     * [MANDATORY] Merchant's site URL where RDP is going to redirect Customer
     * once a final result has been received from Bank/Acquirer
     */
    public void setRedirectUrl(URL redirectUrl) {
        this.redirect_url = redirectUrl;
    }

    public URL getNotifyUrl() {
        return notify_url;
    }

    /**
     * 
     * @param notifyUrl
     * [MANDATORY] Merchant's site URL where a notification will received 
     * once a final result of the payment transaction is acquired.
     */
    public void setNotifyUrl(URL notifyUrl) {
        this.notify_url = notifyUrl;
    }

    public String getBillToForename() {
        return bill_to_forename;
    }

    /**
     * 
     * @param billToForename
     * [CONDITIONAL] It is Mandatory when the acquirer chosen is Cybersource,
     * other than that this field is optional, it is useful for 
     * Fraud Detection System (FDS). 
     * The forename to whom the transaction is being billed.
     */
    public void setBillToForename(String billToForename) {
        this.bill_to_forename = billToForename;
    }

    public String getBillToSurname() {
        return bill_to_surname;
    }

    /**
     * 
     * @param billToSurname
     * [CONDITIONAL] It is Mandatory when the acquirer chosen is Cybersource,
     * other than that this field is optional, it is useful for 
     * Fraud Detection System (FDS). 
     * The surname to whom the transaction is being billed.
     */
    public void setBillToSurname(String billToSurname) {
        this.bill_to_surname = billToSurname;
    }

    public String getBillToAddressCity() {
        return bill_to_address_city;
    }

    /**
     * 
     * @param billToAddressCity
     * [CONDITIONAL] It is Mandatory when the acquirer chosen is Cybersource,
     * other than that this field is optional, it is useful for 
     * Fraud Detection System (FDS). 
     * The city where the transaction is being billed.
     */
    public void setBillToAddressCity(String billToAddressCity) {
        this.bill_to_address_city = billToAddressCity;
    }

    public String getBillToAddressLine1() {
        return bill_to_address_line1;
    }

    /**
     * 
     * @param billToAddressLine1
     * [CONDITIONAL] It is Mandatory when the acquirer chosen is Cybersource,
     * other than that this field is optional, it is useful for 
     * Fraud Detection System (FDS). 
     * The first line of street address where the transaction is being billed.
     */
    public void setBillToAddressLine1(String billToAddressLine1) {
        this.bill_to_address_line1 = billToAddressLine1;
    }

    public String getBillToAddressLine2() {
        return bill_to_address_line2;
    }

    /**
     * 
     * @param billToAddressLine2
     * [OPTIONAL] It is Mandatory when the acquirer chosen is Cybersource,
     * other than that this field is optional, it is useful for 
     * Fraud Detection System (FDS). 
     * The second line of street address where the transaction is being billed.
     * [POSSIBLE VALUE] 2 digit of country code. E.g ID, SG
     */
    public void setBillToAddressLine2(String billToAddressLine2) {
        this.bill_to_address_line2 = billToAddressLine2;
    }

    public String getBillToAddressCountry() {
        return bill_to_address_country;
    }

    /**
     * 
     * @param billToAddressCountry
     * [CONDITIONAL] It is Mandatory when the acquirer chosen is Cybersource,
     * other than that this field is optional, it is useful for 
     * Fraud Detection System (FDS). 
     * The Country where the transaction is being billed.
     */
    public void setBillToAddressCountry(String billToAddressCountry) {
        this.bill_to_address_country = billToAddressCountry;
    }

    public String getBillToAddressState() {
        return bill_to_address_state;
    }

    /**
     * 
     * @param billToAddressState
     * [CONDITIONAL] It is Mandatory when the acquirer chosen is Cybersource,
     * other than that this field is optional, it is useful for 
     * Fraud Detection System (FDS). 
     * The state where the transaction is being billed. (US/Canada only)
     */
    public void setBillToAddressState(String billToAddressState) {
        this.bill_to_address_state = billToAddressState;
    }

    public String getBillToAddressPostalCode() {
        return bill_to_address_postal_code;
    }

    /**
     * 
     * @param billToAddressPostalCode
     * [CONDITIONAL] It is Mandatory when the acquirer chosen is Cybersource,
     * other than that this field is optional, it is useful for 
     * Fraud Detection System (FDS). 
     * The postal code where the transaction is being billed.
     */
    public void setBillToAddressPostalCode(String billToAddressPostalCode) {
        this.bill_to_address_postal_code = billToAddressPostalCode;
    }

    public String getBillToPhone() {
        return bill_to_phone;
    }

    /**
     * 
     * @param billToPhone
     * [CONDITIONAL] It is Mandatory when the acquirer chosen is Cybersource,
     * other than that this field is optional, it is useful for 
     * Fraud Detection System (FDS). 
     * The card holder phone whose the transaction is being filled.
     */
    public void setBillToPhone(String billToPhone) {
        this.bill_to_phone = billToPhone;
    }

    public String getShipToForename() {
        return ship_to_forename;
    }

    /**
     * 
     * @param shipToForename
     * [OPTIONAL] This field is optional, it is useful for Fraud Detection 
     * System (FDS). The forename to whom the item sold is being shipped.
     */
    public void setShipToForename(String shipToForename) {
        this.ship_to_forename = shipToForename;
    }

    public String getShipToSurname() {
        return ship_to_surname;
    }

    /**
     * 
     * @param shipToSurname
     * [OPTIONAL] This field is optional, it is useful for Fraud Detection 
     * System (FDS). The surname to whom the item sold is being shipped.
     */
    public void setShipToSurname(String shipToSurname) {
        this.ship_to_surname = shipToSurname;
    }

    public String getShipToAddressCity() {
        return ship_to_address_city;
    }

    /**
     * 
     * @param shipToAddressCity
     * [OPTIONAL] This field is optional, it is useful for Fraud Detection 
     * System (FDS). The address city to where the item sold is being shipped.
     */
    public void setShipToAddressCity(String shipToAddressCity) {
        this.ship_to_address_city = shipToAddressCity;
    }

    public String getShipToAddressLine1() {
        return ship_to_address_line1;
    }

    /**
     * 
     * @param shipToAddressLine1
     * [OPTIONAL] This field is optional, it is useful for Fraud Detection 
     * System (FDS). The first line of street address to where the item sold 
     * is being shipped.
     */
    public void setShipToAddressLine1(String shipToAddressLine1) {
        this.ship_to_address_line1 = shipToAddressLine1;
    }

    public String getShipToAddressLine2() {
        return ship_to_address_line2;
    }

    /**
     * 
     * @param shipToAddressLine2
     * [OPTIONAL] This field is optional, it is useful for Fraud Detection 
     * System (FDS). The second line of street address to where the item sold 
     * is being shipped.
     */
    public void setShipToAddressLine2(String shipToAddressLine2) {
        this.ship_to_address_line2 = shipToAddressLine2;
    }

    public String getShipToAddressCountry() {
        return ship_to_address_country;
    }

    /**
     * 
     * @param shipToAddressCountry
     * [OPTIONAL] This field is optional, it is useful for Fraud Detection 
     * System (FDS). The Country to where the item sold is being shipped.
     */
    public void setShipToAddressCountry(String shipToAddressCountry) {
        this.ship_to_address_country = shipToAddressCountry;
    }

    public String getShipToAddressState() {
        return ship_to_address_state;
    }

    /**
     * 
     * @param shipToAddressState
     * [OPTIONAL] This field is optional, it is useful for Fraud Detection 
     * System (FDS). The State to where the item sold is being shipped.
     */
    public void setShipToAddressState(String shipToAddressState) {
        this.ship_to_address_state = shipToAddressState;
    }

    public String getShipToAddressPostalCode() {
        return ship_to_address_postal_code;
    }

    /**
     * 
     * @param shipToAddressPostalCode
     * [OPTIONAL] This field is optional, it is useful for Fraud Detection 
     * System (FDS). The postal code to where the item sold is being shipped.
     */
    public void setShipToAddressPostalCode(String shipToAddressPostalCode) {
        this.ship_to_address_postal_code = shipToAddressPostalCode;
    }

    public String getShipToPhone() {
        return ship_to_phone;
    }

    /**
     * 
     * @param shipToPhone
     * [OPTIONAL] This field is optional, it is useful for Fraud Detection 
     * System (FDS). The number to whom the item sold is being shipped.
     */
    public void setShipToPhone(String shipToPhone) {
        this.ship_to_phone = shipToPhone;
    }
}
