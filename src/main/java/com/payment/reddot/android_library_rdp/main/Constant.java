/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.payment.reddot.android_library_rdp.main;

/**
 * @author Red Dot Payment
 */
public class Constant {
    public static int ENVIRONMENT_SANDBOX = 021;
    public static int ENVIRONMENT_LIVE = 022;
    public static int CARD_IO_REQUEST_CODE = 2303;
}
