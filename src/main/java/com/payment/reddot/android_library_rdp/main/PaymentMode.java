/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.payment.reddot.android_library_rdp.main;

/**
 *
 * @author Red Dot Payment
 */
public enum PaymentMode {
    VISA {
      public int asInteger() {
          return 1;
      }
    },
    MASTERCARD {
      public int asInteger() {
          return 2;
      }
    },
    AMEX {
      public int asInteger() {
          return 3;
      }
    },
    JCB {
      public int asInteger() {
          return 4;
      }
    },
    UPOP {
      public int asInteger() {
          return 5;
      }
    },
    DINERS {
      public int asInteger() {
          return 6;
      }
    },
    DISCOVER {
      public int asInteger() {
          return 7;
      }
    },
    LASER {
      public int asInteger() {
          return 8;
      }
    },
    MAESTRO {
      public int asInteger() {
          return 9;
      }
    },
    PAYPAL {
      public int asInteger() {
          return 10;
      }
    },
    ALIPAY {
      public int asInteger() {
          return 11;
      }
    },
    TENPAY {
      public int asInteger() {
          return 12;
      }
    },
    NINETY_NINE_BILL {
      public int asInteger() {
          return 13;
      }
    },
    ENETS {
      public int asInteger() {
          return 14;
      }
    },
    PAYLAH {
      public int asInteger() {
          return 15;
      }
    },
    FLASHPAY {
      public int asInteger() {
          return 16;
      }
    },
    EZ_LINK {
      public int asInteger() {
          return 17;
      }
    },
    CASHCARD {
      public int asInteger() {
          return 18;
      }
    },
    VOUCHER {
      public int asInteger() {
          return 19;
      }
    },
    OTHERS {
      public int asInteger() {
          return 20;
      }
    };
}
