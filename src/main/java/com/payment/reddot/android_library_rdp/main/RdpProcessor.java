/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.payment.reddot.android_library_rdp.main;

import android.app.Activity;
import android.content.Intent;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.payment.reddot.android_library_rdp.AuthorizationActivity;
import com.payment.reddot.android_library_rdp.PaymentPageActivity;
import com.payment.reddot.android_library_rdp.RdpCardIOActivity;
import com.payment.reddot.android_library_rdp.TokenizationActivity;
import com.payment.reddot.android_library_rdp.payment.CreatePaymentResponse;
import com.payment.reddot.android_library_rdp.payment.GetPaymentResult;
import com.payment.reddot.android_library_rdp.payment.PaymentRequestV1;
import com.payment.reddot.android_library_rdp.payment.PaymentRequestV2;
import com.payment.reddot.android_library_rdp.paymentlist.PaymentListResponse;
import com.payment.reddot.android_library_rdp.tokenization.CreateTokenResult;
import com.payment.reddot.android_library_rdp.tokenization.CreateTokenTicketRequest;
import com.payment.reddot.android_library_rdp.tokenization.CreateTokenTicketResponse;
import com.payment.reddot.android_library_rdp.tokenization.DeleteTokenResult;
import com.payment.reddot.android_library_rdp.tokenization.ModifyTokenTicketRequest;
import com.payment.reddot.android_library_rdp.tokenization.ModifyTokenTicketResponse;

import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONException;
import org.json.simple.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.ExecutionException;

/**
 * @author Red Dot Payment
 */
public class RdpProcessor {

    String mid = "", secretKey = "", key = "";
    CreateTokenTicketResponse createTokenTicketResponse = null;
    CreateTokenResult createTokenResult = null, tokenizationPageResult = null;
    ModifyTokenTicketResponse modifyTokenTicketResponse = null;
    DeleteTokenResult deleteTokenResult = null;
    String urlRequestUrl = "";
    String urlHandlingRedirection = "";
    String urlRequestPayment = "";
    String urlHandlingPaymentRedirection = "";
    String urlPaymentChannel = "https://secure-dev.reddotpayment.com/service/Payment_channel_processor/";
    int environment = 0;
    Verificator verificator = new Verificator();

    GetPaymentResult getPaymentResult = null;
    CreatePaymentResponse createPaymentTicketResponse = null;
    PaymentRequestV2 createPaymentRequestV2 = null;
    PaymentListResponse getPaymentList = null;

    /**
     * Constructor and setup credential.
     *
     * @param environment a parameter for choosing environment between test and
     *                    live server.
     * @param mid         an id of merchant.
     * @param key         a key from merchant request.
     * @param secretKey   a secret key between merchant and RDP.
     */
    public RdpProcessor(int environment, String mid, String secretKey, String key) {
        this.environment = environment;
        this.mid = mid;
        this.secretKey = secretKey;
        this.key = key;

        if (environment == Constant.ENVIRONMENT_LIVE) {
            urlRequestUrl = "https://secure.reddotpayment.com/service/token-api";
            urlHandlingRedirection = "https://secure.reddotpayment.com/service/Merchant_processor/query_token_redirection";
            urlRequestPayment = "https://secure.reddotpayment.com/service/payment-api";
            urlHandlingPaymentRedirection = "https://secure.reddotpayment.com/service/Merchant_processor/query_redirection";
        } else if (environment == Constant.ENVIRONMENT_SANDBOX) {
            urlRequestUrl = "https://secure-dev.reddotpayment.com/service/token-api";
            urlHandlingRedirection = "https://secure-dev.reddotpayment.com/service/Merchant_processor/query_token_redirection";
            urlRequestPayment = "https://secure-dev.reddotpayment.com/service/payment-api";
            urlHandlingPaymentRedirection = "https://secure-dev.reddotpayment.com/service/Merchant_processor/query_redirection";
        }
    }

    /**
     * Create ticket for your request. Returns object response from server.
     *
     * @param request object of detail request ticket information from merchant.
     * @return an object response ticket data from RDP which contains URL, error
     * code, etc.
     */
    public CreateTokenTicketResponse getCreateTicketDetails(CreateTokenTicketRequest request) throws IOException {
        Gson gson = new Gson();
        String response = "";
        request.setApiMode("hosted_token_api");
        request.setType("C");
        request.setMid(mid);
        StringBuilder calculateString = new StringBuilder();
        String jsonBeforeSignature = gson.toJson(request);
        TreeMap<String, String> sortedMap = new ObjectMapper().readValue(jsonBeforeSignature, TreeMap.class);
        for (Map.Entry<String, String> treeMap : sortedMap.entrySet()) {
            calculateString.append("" + treeMap.getValue());
        }
        calculateString.append(secretKey);
        JsonElement jsonElement = gson.toJsonTree(request);
        jsonElement.getAsJsonObject().addProperty("signature", verificator.generateHash(calculateString.toString()));
        String jsonAfterSignature = gson.toJson(jsonElement);
        DataProcessor processData = new DataProcessor(jsonAfterSignature, urlRequestUrl);
        try {
            response = processData.execute().get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        createTokenTicketResponse = new CreateTokenTicketResponse(response, secretKey);
        return createTokenTicketResponse;
    }

    /**
     * Create ticket for your request. Returns object response from server.
     *
     * @param request object of detail request ticket information from merchant.
     * @return an object response ticket data from RDP which contains URL, error
     * code, etc.
     */
    public void startTokenizationPageActivity(CreateTokenTicketRequest request, Activity
            activity, int requestCode) throws IOException {
        Gson gson = new Gson();
        String response = "";
        request.setApiMode("hosted_token_api");
        request.setType("C");
        request.setMid(mid);
        StringBuilder calculateString = new StringBuilder();
        String jsonBeforeSignature = gson.toJson(request);
        TreeMap<String, String> sortedMap = new ObjectMapper().readValue(jsonBeforeSignature, TreeMap.class);
        for (Map.Entry<String, String> treeMap : sortedMap.entrySet()) {
            calculateString.append(treeMap.getValue());
        }
        calculateString.append(secretKey);
        JsonElement jsonElement = gson.toJsonTree(request);
        jsonElement.getAsJsonObject().addProperty("signature", verificator.generateHash(calculateString.toString()));
        String jsonAfterSignature = gson.toJson(jsonElement);
        DataProcessor processData = new DataProcessor(jsonAfterSignature, urlRequestUrl);
        try {
            response = processData.execute().get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        createTokenTicketResponse = new CreateTokenTicketResponse(response, secretKey);

        try {
            Intent i = new Intent(activity, TokenizationActivity.class);
            i.putExtra("payment_url", createTokenTicketResponse.getPaymentUrl().toString());
            i.putExtra("mid", mid);
            i.putExtra("secret_key", secretKey);
            i.putExtra("environment", environment);
            i.putExtra("from_sdk", true);
            activity.startActivityForResult(i, requestCode);
        } catch (Exception e) {

        }
    }

    /**
     * Start RDP Payment Page API V2. Returns detail transaction.
     *
     * @param request     object of detail request ticket information from merchant.
     * @param activity    the context of current activity.
     * @param requestCode the requested code for getting result OnActivity method.
     * @return an object response of status transaction.
     */
    public void startPaymentPageV2Activity(PaymentRequestV2 request, Activity
            activity, int requestCode) throws IOException {
        Gson gson = new Gson();
        request.setMid(mid);
        request.setApiMode(ApiMode.REDIRECTION_HOSTED.toString());

        String jsonBeforeSignature = gson.toJson(request);
        Intent i = new Intent(activity, AuthorizationActivity.class);
        i.putExtra("jsonBeforeSignature", jsonBeforeSignature);
        i.putExtra("urlRequestPayment", urlRequestPayment);
        i.putExtra("mid", mid);
        i.putExtra("secret_key", secretKey);
        i.putExtra("environment", environment);
        i.putExtra("from_sdk", true);
        i.putExtra("from_auto_intent", true);
        activity.startActivityForResult(i, requestCode);
    }

    /**
     * Start RDP Payment Page API V1. Returns detail transaction.
     *
     * @param request     object of detail request ticket information from merchant.
     * @param activity    the context of current activity.
     * @param requestCode the requested code for getting result OnActivity method.
     * @return an object response of status transaction.
     */
    public void startPaymentPageV1Activity(PaymentRequestV1 request, Activity
            activity, int requestCode) throws IOException {

        HashMap<String, String> parameters = new HashMap<String, String>();
        if (request.getMerchant_reference() != null)
            parameters.put("merchant_reference", "" + request.getMerchant_reference());
        if (request.getAmount() != null) parameters.put("amount", "" + request.getAmount());
        if (request.getCurrency_code() != null)
            parameters.put("currency_code", "" + request.getCurrency_code());
        if (request.getEmail() != null) parameters.put("email", "" + request.getEmail());
        if (request.getFirst_name() != null)
            parameters.put("first_name", "" + request.getFirst_name());
        parameters.put("key", "" + key);
        if (request.getLast_name() != null)
            parameters.put("last_name", "" + request.getLast_name());
        parameters.put("merchant_id", "" + mid);
        if (request.getNotify_url() != null)
            parameters.put("notify_url", "" + request.getNotify_url());
        if (request.getOrder_number() != null)
            parameters.put("order_number", "" + request.getOrder_number());
        if (request.getReturn_url() != null)
            parameters.put("return_url", "" + request.getReturn_url());
        if (request.getTransaction_type() != null)
            parameters.put("transaction_type", "" + request.getTransaction_type());
        if (request.getPaymentpage_style() != null)
            parameters.put("paymentpage_style", "" + request.getPaymentpage_style());
        if (request.getPromo_code() != null)
            parameters.put("promo_code", "" + request.getPromo_code());
        if (request.getAddress_line1() != null)
            parameters.put("address_line1", "" + request.getAddress_line1());
        if (request.getAddress_line2() != null)
            parameters.put("address_line2", "" + request.getAddress_line2());
        if (request.getAddress_city() != null)
            parameters.put("address_city", "" + request.getAddress_city());
        if (request.getAddress_portal_code() != null)
            parameters.put("address_postal_code", "" + request.getAddress_portal_code());
        if (request.getAddress_state() != null)
            parameters.put("address_state", "" + request.getAddress_state());
        if (request.getAddress_country() != null)
            parameters.put("address_country", "" + request.getAddress_country());
        if (request.getMerchant_data1() != null)
            parameters.put("merchant_data1", "" + request.getMerchant_data1());
        if (request.getMerchant_data2() != null)
            parameters.put("merchant_data2", "" + request.getMerchant_data2());
        if (request.getMerchant_data3() != null)
            parameters.put("merchant_data3", "" + request.getMerchant_data3());
        if (request.getMerchant_data4() != null)
            parameters.put("merchant_data4", "" + request.getMerchant_data4());
        if (request.getMerchant_data5() != null)
            parameters.put("merchant_data5", "" + request.getMerchant_data5());
        if (request.getToken_id() != null) parameters.put("token_id", "" + request.getToken_id());
        if (request.getOrder_timeout() != null)
            parameters.put("order_timeout", "" + request.getOrder_timeout());

        Intent i = new Intent(activity, PaymentPageActivity.class);
        i.putExtra("parameters", parameters);
        i.putExtra("secret_key", "" + secretKey);
        i.putExtra("environment", environment);
        activity.startActivityForResult(i, requestCode);
    }

    /**
     * Start RDP Payment Page API V1 via Card IO. Returns detail transaction.
     *
     * @param request     object of detail request ticket information from merchant.
     * @param activity    the context of current activity.
     * @param requestCode the requested code for getting result OnActivity method.
     * @return an object response of status transaction.
     */
    public void startCardIOPaymentPageV1Activity(PaymentRequestV1 request, Activity
            activity, int requestCode) throws IOException {

        HashMap<String, String> parameters = new HashMap<String, String>();
        if (request.getMerchant_reference() != null)
            parameters.put("merchant_reference", "" + request.getMerchant_reference());
        if (request.getAmount() != null) parameters.put("amount", "" + request.getAmount());
        if (request.getCurrency_code() != null)
            parameters.put("currency_code", "" + request.getCurrency_code());
        if (request.getEmail() != null) parameters.put("email", "" + request.getEmail());
        parameters.put("key", "" + key);
        parameters.put("merchant_id", "" + mid);
        if (request.getNotify_url() != null)
            parameters.put("notify_url", "" + request.getNotify_url());
        if (request.getOrder_number() != null)
            parameters.put("order_number", "" + request.getOrder_number());
        if (request.getReturn_url() != null)
            parameters.put("return_url", "" + request.getReturn_url());
        if (request.getTransaction_type() != null)
            parameters.put("transaction_type", "" + request.getTransaction_type());
        if (request.getPaymentpage_style() != null)
            parameters.put("paymentpage_style", "" + request.getPaymentpage_style());
        if (request.getPromo_code() != null)
            parameters.put("promo_code", "" + request.getPromo_code());
        if (request.getAddress_line1() != null)
            parameters.put("address_line1", "" + request.getAddress_line1());
        if (request.getAddress_line2() != null)
            parameters.put("address_line2", "" + request.getAddress_line2());
        if (request.getAddress_city() != null)
            parameters.put("address_city", "" + request.getAddress_city());
        if (request.getAddress_portal_code() != null)
            parameters.put("address_postal_code", "" + request.getAddress_portal_code());
        if (request.getAddress_state() != null)
            parameters.put("address_state", "" + request.getAddress_state());
        if (request.getAddress_country() != null)
            parameters.put("address_country", "" + request.getAddress_country());
        if (request.getMerchant_data1() != null)
            parameters.put("merchant_data1", "" + request.getMerchant_data1());
        if (request.getMerchant_data2() != null)
            parameters.put("merchant_data2", "" + request.getMerchant_data2());
        if (request.getMerchant_data3() != null)
            parameters.put("merchant_data3", "" + request.getMerchant_data3());
        if (request.getMerchant_data4() != null)
            parameters.put("merchant_data4", "" + request.getMerchant_data4());
        if (request.getMerchant_data5() != null)
            parameters.put("merchant_data5", "" + request.getMerchant_data5());
        if (request.getToken_id() != null) parameters.put("token_id", "" + request.getToken_id());
        if (request.getOrder_timeout() != null)
            parameters.put("order_timeout", "" + request.getOrder_timeout());

        Intent i = new Intent(activity, PaymentPageActivity.class);
        i.putExtra("parameters", parameters);
        i.putExtra("secret_key", "" + secretKey);
        i.putExtra("environment", environment);
        i.putExtra("scan_mode", true);
        activity.startActivityForResult(i, requestCode);
    }

    /**
     * Start RDP Payment Page API V2 with Card IO Scanner. Returns detail transaction.
     *
     * @param request     object of detail request ticket information from merchant.
     * @param activity    the context of current activity.
     * @param requestCode the requested code for getting result OnActivity method.
     * @return an object response of status transaction.
     */
    public void startCardIOPaymentPageV2Activity(PaymentRequestV2 request, Activity
            activity, int requestCode) throws IOException {
        Gson gson = new Gson();
        request.setMid(mid);
        request.setApiMode(ApiMode.REDIRECTION_SOP.toString());

        String jsonBeforeSignature = gson.toJson(request);
        Intent i = new Intent(activity, RdpCardIOActivity.class);
        i.putExtra("jsonBeforeSignature", jsonBeforeSignature);
        i.putExtra("requestCode", requestCode);
        i.putExtra("urlRequestPayment", urlRequestPayment);
        i.putExtra("mid", mid);
        i.putExtra("secret_key", secretKey);
        i.putExtra("environment", environment);
        i.putExtra("from_sdk", true);
        activity.startActivityForResult(i, requestCode);
    }

    /**
     * Asking the details from server by sending transactionId. Returns object.
     *
     * @param transactionId an id of transaction from merchant to ask the
     *                      details.
     * @return response data from server.
     */
    public CreateTokenResult getTokenResult(String transactionId) throws JSONException {
        String response = "";
        StringBuilder calculateString = new StringBuilder();
        JSONObject postParameters = new JSONObject();
        postParameters.put("request_mid", mid);
        postParameters.put("transaction_id", transactionId);

        TreeMap<String, String> calculateSignature = new TreeMap<>(postParameters);
        for (Map.Entry<String, String> treeMap : calculateSignature.entrySet()) {
            calculateString.append(treeMap.getValue());
        }
        calculateString.append(secretKey);
        postParameters.put("signature", verificator.generateHash(calculateString.toString()));
        DataProcessor processData = new DataProcessor(postParameters.toString(), urlHandlingRedirection);
        try {
            response = processData.execute().get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        createTokenResult = new CreateTokenResult(response, secretKey);
        return createTokenResult;
    }

    /**
     * Asking the details inquiry from server by sending transactionId. Returns
     * object.
     *
     * @param bodyResponse an id of transaction from merchant to ask the
     *                     details.
     * @return response data from server.
     */
    public CreateTokenResult translateRawIntoResult(String bodyResponse) {
        createTokenResult = new CreateTokenResult(bodyResponse, secretKey);
        return createTokenResult;
    }

    /**
     * Create ticket for your request. Returns object response from server.
     *
     * @param request object of detail request ticket information from merchant.
     * @return an object response ticket data from RDP which contains URL, error
     * code, etc.
     */
    public ModifyTokenTicketResponse getCreateTicketDetails(ModifyTokenTicketRequest request) throws IOException {
        Gson gson = new Gson();
        String response = "";
        request.setApiMode("hosted_token_api");
        request.setType("M");
        request.setMid(mid);
        StringBuilder calculateString = new StringBuilder();
        String jsonBeforeSignature = gson.toJson(request);
        TreeMap<String, String> sortedMap = new ObjectMapper().readValue(jsonBeforeSignature, TreeMap.class);
        for (Map.Entry<String, String> treeMap : sortedMap.entrySet()) {
            calculateString.append(treeMap.getValue());
        }
        calculateString.append(secretKey);
        JsonElement jsonElement = gson.toJsonTree(request);
        jsonElement.getAsJsonObject().addProperty("signature", verificator.generateHash(calculateString.toString()));
        String jsonAfterSignature = gson.toJson(jsonElement);
        DataProcessor processData = new DataProcessor(jsonAfterSignature, urlRequestUrl);
        try {
            response = processData.execute().get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        modifyTokenTicketResponse = new ModifyTokenTicketResponse(response, secretKey);
        return modifyTokenTicketResponse;
    }

    /**
     * Directly consume delete API token from server. Returns object response
     * from server.
     *
     * @param orderId    an id of the order.
     * @param payerId    the token which one to delete.
     * @param payerEmail the email of one who pay the transaction.
     * @return response code from RDP.
     */
    public DeleteTokenResult deleteToken(String orderId, String payerId, String payerEmail) throws JSONException {
        String response = "";
        StringBuilder calculateString = new StringBuilder();
        JSONObject postParameters = new JSONObject();
        postParameters.put("mid", mid);
        postParameters.put("order_id", orderId);
        postParameters.put("api_mode", "direct_token_api");
        postParameters.put("payer_id", payerId);
        if (payerEmail.equals("")) {

        } else {
            postParameters.put("payer_email", payerEmail);
        }
        postParameters.put("type", "R");
        TreeMap<String, String> calculateSignature = new TreeMap<>(postParameters);
        for (Map.Entry<String, String> treeMap : calculateSignature.entrySet()) {
            calculateString.append(treeMap.getValue());
        }
        calculateString.append(secretKey);
        postParameters.put("signature", verificator.generateHash(calculateString.toString()));
        DataProcessor processData = new DataProcessor(postParameters.toString(), urlRequestUrl);
        try {
            response = processData.execute().get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        deleteTokenResult = new DeleteTokenResult(response, secretKey);
        return deleteTokenResult;
    }

    /**
     * Create ticket for making a payment. Returns object response from server.
     *
     * @param request object of detail request ticket information from merchant.
     * @return an object response ticket data from RDP which contains URL, error
     * code, etc.
     */
    public CreatePaymentResponse getPaymentTicketDetails(PaymentRequestV2 request) throws IOException {
        Gson gson = new Gson();
        String response = "";
        request.setApiMode(ApiMode.REDIRECTION_HOSTED.toString());
        request.setMid(mid);
        StringBuilder calculateString = new StringBuilder();

        /* caller_uri temporary validation check */
        if (request.getCallerApplicationURI() == null || request.getCallerApplicationURI().toString().equals("")) {
            createPaymentTicketResponse
                    = new CreatePaymentResponse(
                    "{\"response_code\":\"-1019\", "
                            + "\"response_msg\":"
                            + "\"caller app URI is mandatory\"}", secretKey);

            return createPaymentTicketResponse;
        }
        /* caller_uri temporary validation check */

        calculateString.append(request.getMid());
        calculateString.append(request.getOrderId());
        calculateString.append(request.getPaymentType());
        calculateString.append(request.getAmount());
        calculateString.append(request.getCcy());
        calculateString.append(request.getPayerId() == null ? "" : request.getPayerId());
        calculateString.append(secretKey);
        JsonElement jsonElement = gson.toJsonTree(request);

        if (jsonElement.getAsJsonObject().get("payment_channel") == null || jsonElement.getAsJsonObject().get("payment_channel").toString().equals("0")) {
            jsonElement.getAsJsonObject().remove("payment_channel");
        }
        jsonElement.getAsJsonObject().addProperty("signature", verificator.generateHash(calculateString.toString()));
        String jsonAfterSignature = gson.toJson(jsonElement);

        DataProcessor processData = new DataProcessor(jsonAfterSignature, urlRequestPayment);
        try {
            response = processData.execute().get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        createPaymentTicketResponse = new CreatePaymentResponse(response, secretKey);
        return createPaymentTicketResponse;
    }

    /**
     * Create ticket for making a payment with Card IO Scanner. Returns object response from server.
     *
     * @param request object of detail request ticket information from merchant.
     * @return an object response ticket data from RDP which contains URL, error
     * code, etc.
     */
    public void getPaymentTicketDetailsViaCardIOActivity(PaymentRequestV2 request, Activity
            activity, int requestCode) throws IOException {
        Gson gson = new Gson();
        request.setApiMode(ApiMode.REDIRECTION_SOP.toString());
        request.setMid(mid);

        String jsonBeforeSignature = gson.toJson(request);
        Intent i = new Intent(activity, RdpCardIOActivity.class);
        i.putExtra("jsonBeforeSignature", jsonBeforeSignature);
        i.putExtra("requestCode", requestCode);
        i.putExtra("urlRequestPayment", urlRequestPayment);
        i.putExtra("mid", mid);
        i.putExtra("secret_key", secretKey);
        i.putExtra("environment", environment);
        i.putExtra("customizable", true);
        activity.startActivityForResult(i, requestCode);
    }

    /**
     * Asking the Payment Result from server for the respective payment with the
     * corresponding transaction-id.
     *
     * @param transactionId an id of transaction from merchant to ask the
     *                      details.
     * @return response data from server.
     */
    public GetPaymentResult getPaymentResult(String transactionId) throws JSONException {
        String response = "";
        StringBuilder calculateString = new StringBuilder();
        JSONObject postParameters = new JSONObject();
        postParameters.put("transaction_id", transactionId);
        postParameters.put("request_mid", mid);
        TreeMap<String, String> calculateSignature = new TreeMap<>(postParameters);
        for (Map.Entry<String, String> treeMap : calculateSignature.entrySet()) {
            calculateString.append(treeMap.getValue());
        }
        calculateString.append(secretKey);
        postParameters.put("signature", verificator.generateHash(calculateString.toString()));
        DataProcessor processData = new DataProcessor(postParameters.toString(), urlHandlingPaymentRedirection);
        try {
            response = processData.execute().get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        getPaymentResult = new GetPaymentResult(response, secretKey);
        return getPaymentResult;
    }

    /**
     * Handling the conversion of raw string of JSON from the Push-Notification
     * flow into a comprehensive result Object.
     *
     * @param bodyResponse is raw data merchant got from push notification.
     * @return response data from server.
     */
    public GetPaymentResult translateRawIntoPaymentResult(String bodyResponse) {
        getPaymentResult = new GetPaymentResult(bodyResponse, secretKey);
        return getPaymentResult;
    }

    /**
     * @return response data from server.
     */
    public PaymentListResponse getPaymentListResponse() throws JSONException {
        String response = "";
        StringBuilder calculateString = new StringBuilder();
        JSONObject postParameters = new JSONObject();
        postParameters.put("random_seed", "seed_" + System.currentTimeMillis());
        postParameters.put("mid", mid);
        TreeMap<String, String> calculateSignature = new TreeMap<>(postParameters);
        for (Map.Entry<String, String> treeMap : calculateSignature.entrySet()) {
            calculateString.append(treeMap.getValue());
        }
        calculateString.append(secretKey);
        postParameters.put("signature", verificator.generateHash(calculateString.toString()));
        DataProcessor processData = new DataProcessor(postParameters.toString(), urlPaymentChannel);
        try {
            response = processData.execute().get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        getPaymentList = new PaymentListResponse(response, secretKey);
        return getPaymentList;
    }
}
