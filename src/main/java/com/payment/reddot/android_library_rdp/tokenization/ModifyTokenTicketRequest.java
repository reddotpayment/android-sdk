/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.payment.reddot.android_library_rdp.tokenization;

import java.net.URL;

/**
 *
 * @author Red Dot Payment
 */
public class ModifyTokenTicketRequest {

    private String token_id;
    private String mid;
    private String type;
    private String api_mode;
    private String ccy;
    private String order_id;
    private String payer_id;
    private String payer_email;
    private URL back_url;
    private URL redirect_url;
    private URL notify_url;
    private String bill_to_forename;
    private String bill_to_surname;
    private String bill_to_address_city;
    private String bill_to_address_line1;
    private String bill_to_address_line2;
    private String bill_to_address_country;
    private String bill_to_address_state;
    private String bill_to_address_postal_code;
    private String bill_to_phone;

    public ModifyTokenTicketRequest() {

    }

    public String getPayerEmail() {
        return payer_email;
    }

    /**
     *
     * @param payer_email [CONDITIONAL] Merchant defined payer email or
     * customer email.
     */
    public void setPayerEmail(String payer_email) {
        this.payer_email = payer_email;
    }
    
    public String getTokenId() {
        return token_id;
    }

    /**
     *
     * @param token_id [MANDATORY] The token-id created by the source token
     * processor.
     */
    public void setTokenId(String token_id) {
        this.token_id = token_id;
    }

    public String getMid() {
        return mid;
    }

    /**
     *
     * @param mid [MANDATORY] The merchant ID given by RDP when setting up an
     * account.
     */
    public void setMid(String mid) {
        this.mid = mid;
    }

    public String getType() {
        return type;
    }

    /**
     *
     * @param type [SET-BY-FUNCTION] determines whether it is a create / modify
     * <br>
     * possible values<br>
     * C : Create<br>
     * M : Modify<br>
     */
    public void setType(String type) {
        this.type = type;
    }

    public String getApiMode() {
        return api_mode;
    }

    /**
     *
     * @param api_mode [SET-BY-FUNCTION] whether using direct / hosted API.<br>
     * possible values : direct_token_api, hosted_token_api
     */
    public void setApiMode(String api_mode) {
        this.api_mode = api_mode;
    }

    public String getCcy() {
        return ccy;
    }

    /**
     *
     * @param ccy [MANDATORY] In 3 digits ISO-4217 Alphabetical Currency Code
     * format<br>
     * e.g. SGD
     */
    public void setCcy(String ccy) {
        this.ccy = ccy;
    }

    public String getOrderId() {
        return order_id;
    }

    /**
     *
     * @param orderId [MANDATORY][VARCHAR(20)] Merchant defined order-id for the
     * transaction. Used for identifying the transaction request. Suggested to
     * be of unique values. Merchant can request enforcement of unique order_id
     * from RDP (where repeated order_id is to be rejected.) Note: By default
     * RDP allowed non-unique order-id
     */
    public void setOrderId(String orderId) {
        this.order_id = orderId;
    }

    public String getPayerId() {
        return payer_id;
    }

    /**
     *
     * @param payerId [OPTIONAL][VARCHAR(100)] Merchant defined payer ID or
     * customer ID. Used to identify a unique merchant's customer.
     */
    public void setPayerId(String payerId) {
        this.payer_id = payerId;
    }

    public URL getBackUrl() {
        return back_url;
    }

    /**
     *
     * @param backUrl [MANDATORY][URL] Merchant's site URL where customer is to
     * be redirected when they chose to press "cancel" button on RDP's
     * tokenization page.
     */
    public void setBackUrl(URL backUrl) {
        this.back_url = backUrl;
    }

    public URL getRedirectUrl() {
        return redirect_url;
    }

    /**
     *
     * @param redirectUrl [MANDATORY][URL] Merchant's site URL where RDP is
     * going to redirect Customer once a final result has been received from
     * Bank/Acquirer tokenization system.
     */
    public void setRedirectUrl(URL redirectUrl) {
        this.redirect_url = redirectUrl;
    }

    public URL getNotifyUrl() {
        return notify_url;
    }

    /**
     *
     * @param notifyUrl [MANDATORY][URL] Merchant's site URL where a
     * notification will receive once a final result of the tokenization process
     * is done.
     */
    public void setNotifyUrl(URL notifyUrl) {
        this.notify_url = notifyUrl;
    }

    public String getBillToForename() {
        return bill_to_forename;
    }

    /**
     *
     * @param billToForename [MANDATORY][VARCHAR(60)]
     */
    public void setBillToForename(String billToForename) {
        this.bill_to_forename = billToForename;
    }

    public String getBillToSurname() {
        return bill_to_surname;
    }

    /**
     *
     * @param billToSurname [MANDATORY][VARCHAR(60)]
     */
    public void setBillToSurname(String billToSurname) {
        this.bill_to_surname = billToSurname;
    }

    public String getBillToAddressCity() {
        return bill_to_address_city;
    }

    /**
     *
     * @param billToAddressCity [MANDATORY][VARCHAR(50)]
     */
    public void setBillToAddressCity(String billToAddressCity) {
        this.bill_to_address_city = billToAddressCity;
    }

    public String getBillToAddressLine1() {
        return bill_to_address_line1;
    }

    /**
     *
     * @param billToAddressLine1 [MANDATORY][VARCHAR(60)]
     */
    public void setBillToAddressLine1(String billToAddressLine1) {
        this.bill_to_address_line1 = billToAddressLine1;
    }

    public String getBillToAddressLine2() {
        return bill_to_address_line2;
    }

    /**
     *
     * @param billToAddressLine2 [MANDATORY][VARCHAR(60)]
     */
    public void setBillToAddressLine2(String billToAddressLine2) {
        this.bill_to_address_line2 = billToAddressLine2;
    }

    public String getBillToAddressCountry() {
        return bill_to_address_country;
    }

    /**
     *
     * @param billToAddressCountry [MANDATORY][VARCHAR(2)] - Two-character ISO
     * Country Code
     */
    public void setBillToAddressCountry(String billToAddressCountry) {
        this.bill_to_address_country = billToAddressCountry;
    }

    public String getBillToAddressState() {
        return bill_to_address_state;
    }

    /**
     *
     * @param billToAddressPostalCode [CONDITIONAL : US country
     * only][VARCHAR(2)] - Two-character ISO State and Province Code
     */
    public void setBillToAddressState(String billToAddressState) {
        this.bill_to_address_state = billToAddressState;
    }

    public String getBillToAddressPostalCode() {
        return bill_to_address_postal_code;
    }

    /**
     *
     * @param billToAddressPostalCode [MANDATORY][VARCHAR(10)]
     */
    public void setBillToAddressPostalCode(String billToAddressPostalCode) {
        this.bill_to_address_postal_code = billToAddressPostalCode;
    }

    public String getBillToPhone() {
        return bill_to_phone;
    }

    /**
     *
     * @param billToPhone [OPTIONAL] [VARCHAR(15)]
     */
    public void setBillToPhone(String billToPhone) {
        this.bill_to_phone = billToPhone;
    }
}
