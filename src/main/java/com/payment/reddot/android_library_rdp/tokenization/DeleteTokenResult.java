/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.payment.reddot.android_library_rdp.tokenization;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.payment.reddot.android_library_rdp.main.Verificator;

import java.lang.reflect.Type;
import java.util.Map;
import java.util.TreeMap;

/**
 *
 * @author Red Dot Payment
 */
public class DeleteTokenResult {

    private String mid;
    private String transaction_id;
    private String order_id;
    private String payer_id;
    private String token_id;
    private String response_code;
    private String response_msg;
    private String acquirer_transaction_id;
    private String acquirer_response_code;
    private String acquirer_response_msg;
    private String created_timestamp;
    private String request_timestamp;
    private String first6;
    private String last4;
    private String transaction_type;
    private String signature;

    public DeleteTokenResult(String bodyResponse, String secret_key) {
        StringBuilder calculateString = new StringBuilder();
        Verificator verificator = new Verificator();
        Gson gson = new Gson();
        Type stringStringMap = new TypeToken<Map<String, String>>() {
        }.getType();
        Map<String, String> map = gson.fromJson(bodyResponse, stringStringMap);
        TreeMap<String, String> sortedParameter = new TreeMap<>(map);
        for (Map.Entry<String, String> treeMap : sortedParameter.entrySet()) {
            if (treeMap.getKey().equals("signature")) {
            } else {
                calculateString.append(treeMap.getValue());
            }
        }
        calculateString.append(secret_key);
        try {
            if (map.get("signature").toString().equals(verificator.generateHash(calculateString.toString()))) {
                try {
                    this.mid = map.get("mid").toString();
                } catch (Exception e) {
                }
                try {
                    this.transaction_id = map.get("transaction_id").toString();
                } catch (Exception e) {
                }
                try {
                    this.order_id = map.get("order_id").toString();
                } catch (Exception e) {
                }
                try {
                    this.payer_id = map.get("payer_id").toString();
                } catch (Exception e) {
                }
                try {
                    this.token_id = map.get("token_id").toString();
                } catch (Exception e) {
                }
                try {
                    this.response_code = map.get("response_code").toString();
                } catch (Exception e) {
                }
                try {
                    this.response_msg = map.get("response_msg").toString();
                } catch (Exception e) {
                }
                try {
                    this.acquirer_transaction_id = map.get("acquirer_transaction_id").toString();
                } catch (Exception e) {
                }
                try {
                    this.acquirer_response_code = map.get("acquirer_response_code").toString();
                } catch (Exception e) {
                }
                try {
                    this.acquirer_response_msg = map.get("acquirer_response_msg").toString();
                } catch (Exception e) {
                }
                try {
                    this.created_timestamp = map.get("created_timestamp").toString();
                } catch (Exception e) {
                }
                try {
                    this.request_timestamp = map.get("request_timestamp").toString();
                } catch (Exception e) {
                }
                try {
                    this.first6 = map.get("first_6").toString();
                } catch (Exception e) {
                }
                try {
                    this.last4 = map.get("last_4").toString();
                } catch (Exception e) {
                }
                try {
                    this.transaction_type = map.get("transaction_type").toString();
                } catch (Exception e) {
                }
                try {
                    this.signature = map.get("signature").toString();
                } catch (Exception e) {
                }
            }
        } catch (Exception ex) {
            try {
                this.mid = map.get("mid").toString();
            } catch (Exception e) {
            }
            try {
                this.transaction_id = map.get("transaction_id").toString();
            } catch (Exception e) {
            }
            try {
                this.order_id = map.get("order_id").toString();
            } catch (Exception e) {
            }
            try {
                this.payer_id = map.get("payer_id").toString();
            } catch (Exception e) {
            }
            try {
                this.token_id = map.get("token_id").toString();
            } catch (Exception e) {
            }
            try {
                this.response_code = map.get("response_code").toString();
            } catch (Exception e) {
            }
            try {
                this.response_msg = map.get("response_msg").toString();
            } catch (Exception e) {
            }
            try {
                this.acquirer_transaction_id = map.get("acquirer_transaction_id").toString();
            } catch (Exception e) {
            }
            try {
                this.acquirer_response_code = map.get("acquirer_response_code").toString();
            } catch (Exception e) {
            }
            try {
                this.acquirer_response_msg = map.get("acquirer_response_msg").toString();
            } catch (Exception e) {
            }
            try {
                this.created_timestamp = map.get("created_timestamp").toString();
            } catch (Exception e) {
            }
            try {
                this.request_timestamp = map.get("request_timestamp").toString();
            } catch (Exception e) {
            }
            try {
                this.first6 = map.get("first_6").toString();
            } catch (Exception e) {
            }
            try {
                this.last4 = map.get("last_4").toString();
            } catch (Exception e) {
            }
            try {
                this.transaction_type = map.get("transaction_type").toString();
            } catch (Exception e) {
            }
            try {
                this.signature = map.get("signature").toString();
            } catch (Exception e) {
            }
        }
    }

    /**
     *
     * @return mid [CONDITIONAL] The merchant ID given by RDP when setting up an
     * account.
     */
    public String getMid() {
        return mid;
    }

    /**
     *
     * @return transaction_id [CONDITIONAL] The RDP generated unique
     * transaction-id, which is used heavily for identifying the resulted
     * transaction in RDP system.
     */
    public String getTransactionId() {
        return transaction_id;
    }

    /**
     *
     * @return order_id [CONDITIONAL] An echo back to Merchant’s order-id for
     * the transaction as the identifier of the transaction.
     */
    public String getOrderId() {
        return order_id;
    }

    /**
     *
     * @return payer_id [CONDITIONAL] Merchant defined payer ID or customer ID.
     * Used to identify a unique merchant's customer.
     */
    public String getPayerId() {
        return payer_id;
    }

    /**
     *
     * @return token_id [CONDITIONAL] The token-id created by the source token
     * processor.
     */
    public String getTokenId() {
        return token_id;
    }

    /**
     *
     * @return response_code [MANDATORY] Flag which defines whether the
     * transaction is accepted, or has an error in request, or rejected by
     * Acquirer (e.g. Bank)
     * <br> 0: success – accepted transaction
     * <br> -1: bank / acquirer rejection others: request-error
     */
    public String getResponseCode() {
        return response_code;
    }

    /**
     *
     * @return response_msg [MANDATORY] Description on the response-code.
     */
    public String getResponseMsg() {
        return response_msg;
    }

    /**
     *
     * @return acquirer_transaction_id [CONDITIONAL] Transaction ID created by
     * acquirer.
     */
    public String getAcquirerTransactionId() {
        return acquirer_transaction_id;
    }

    /**
     *
     * @return acquirer_response_code [CONDITIONAL] Response code from acquirer.
     * Format is specific to each Acquirer.
     */
    public String getAcquirerResponseCode() {
        return acquirer_response_code;
    }

    /**
     *
     * @return acquirer_response_msg [CONDITIONAL] Description of the response
     * code.
     */
    public String getAcquirerResponseMsg() {
        return acquirer_response_msg;
    }

    /**
     *
     * @return created_timestamp [CONDITIONAL] The date-time when the response
     * is created. In a 24 hour format. Timezone is using (UTC+08:00) Kuala
     * Lumpur, Singapore. e.g. 2015-11-14 12:33:27
     */
    public String getCreatedTimestamp() {
        return created_timestamp;
    }

    /**
     *
     * @return request_timestamp [CONDITIONAL] The date-time when the request is
     * created. In a 24 hour format. Timezone is using (UTC+08:00) Kuala Lumpur,
     * Singapore. e.g. 2015-11-14 12:33:27
     */
    public String getRequestTimestamp() {
        return request_timestamp;
    }

    /**
     *
     * @return first6 [CONDITIONAL] First 6 digit of the Card being tokenized.
     */
    public String getFirst6() {
        return first6;
    }

    /**
     *
     * @return last4 [CONDITIONAL] Last 4 digit of the Card being tokenized.
     */
    public String getLast4() {
        return last4;
    }

    /**
     *
     * @return transaction_type [CONDITIONAL] [VARCHAR(1)]<br>
     * C: create token<br>
     * M: modify token<br>
     * R: remove token<br>
     */
    public String getTransactionType() {
        return transaction_type;
    }

    /**
     *
     * @return signature [CONDITIONAL] Signature generated by RDP.
     */
    public String getSignature() {
        return signature;
    }

}
