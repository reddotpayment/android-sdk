/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.payment.reddot.library_rdp.paymentlist;

import com.google.gson.annotations.SerializedName;

/**
 * @author Red Dot Payment
 */
public class PaymentListDetail {

    @SerializedName("payment_channel")
    private String payment_channel;

    @SerializedName("description")
    private String description;

    @SerializedName("mid")
    private String mid;

    @SerializedName("logo_url")
    private String logo_url;

    public final String getPaymentChannel() {
        return this.payment_channel;
    }

    public final String getDescription() {
        return this.description;
    }

    public final String getMid() {
        return this.mid;
    }

    public final String getLogoUrl() {
        return this.logo_url;
    }
}
