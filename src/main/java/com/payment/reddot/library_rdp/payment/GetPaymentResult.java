/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.payment.reddot.library_rdp.payment;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.payment.reddot.library_rdp.main.Verificator;

import java.lang.reflect.Type;
import java.util.Map;
import java.util.TreeMap;

/**
 *
 * @author Red Dot Payment
 */
public class GetPaymentResult {

    private String payer_id;
    private String response_code;
    private String response_msg;
    private String mid;
    private String request_mid;
    private String request_amount;
    private String request_ccy;
    private String authorized_amount;
    private String authorized_ccy;
    private String acquirer_authorized_amount;
    private String acquirer_authorized_ccy;
    private String merchant_reference;
    private String order_id;
    private String transaction_id;
    private String transaction_type;
    private String created_timestamp;
    private String acquirer_response_code;
    private String request_timestamp;
    private String acquirer_response_msg;
    private String signature;
    private String acquirer_created_timestamp;
    private String acquirer_transaction_id;
    private String acquirer_authorization_code;
    private String first_6;
    private String last_4;
    private String payer_name;
    private String exp_date;
    private String payment_mode;
    private String card_number;

    /**
     * @param response The raw string of JSON response from web service API.
     * @param secret_key The secret-key which is used to generate the signature
     * for later validate the signature, in case of wrong signature all members
     * will be null.
     */
    public GetPaymentResult(String response, String secret_key) {
        StringBuilder calculateString = new StringBuilder();
        Verificator verificator = new Verificator();
        Gson gson = new Gson();
        Type stringStringMap = new TypeToken<Map<String, String>>() {
        }.getType();
        Map<String, String> map = gson.fromJson(response, stringStringMap);
        TreeMap<String, String> sortedParameter = new TreeMap<>(map);
        StringBuilder temp = new StringBuilder();
        for (Map.Entry<String, String> treeMap : sortedParameter.entrySet()) {
            if (treeMap.getKey().equals("signature")) {
            } else {
                calculateString.append(treeMap.getValue());
                temp.append(treeMap.getKey());
            }
        }
        calculateString.append(secret_key);
        try {
            if (map.get("signature").toString().equals(verificator.generateHash(calculateString.toString()))) {
                try {
                    this.mid = map.get("mid").toString();
                } catch (Exception e) {
                }
                try {
                    this.card_number = map.get("card_number").toString();
                } catch (Exception e) {
                }
                try {
                    this.acquirer_authorized_amount = map.get("acquirer_authorized_amount").toString();
                } catch (Exception e) {
                }
                try {
                    this.request_timestamp = map.get("request_timestamp").toString();
                } catch (Exception e) {
                }
                try {
                    this.acquirer_authorized_ccy = map.get("acquirer_authorized_ccy").toString();
                } catch (Exception e) {
                }
                try {
                    this.acquirer_created_timestamp = map.get("acquirer_created_timestamp").toString();
                } catch (Exception e) {
                }
                try {
                    this.acquirer_transaction_id = map.get("acquirer_transaction_id").toString();
                } catch (Exception e) {
                }
                try {
                    this.acquirer_authorization_code = map.get("acquirer_authorization_code").toString();
                } catch (Exception e) {
                }
                try {
                    this.last_4 = map.get("last_4").toString();
                } catch (Exception e) {
                }
                try {
                    this.first_6 = map.get("first_6").toString();
                } catch (Exception e) {
                }
                try {
                    this.payment_mode = map.get("payment_mode").toString();
                } catch (Exception e) {
                }
                try {
                    this.merchant_reference = map.get("merchant_reference").toString();
                } catch (Exception e) {
                }
                try {
                    this.authorized_ccy = map.get("authorized_ccy").toString();
                } catch (Exception e) {
                }
                try {
                    this.authorized_amount = map.get("authorized_amount").toString();
                } catch (Exception e) {
                }
                try {
                    this.request_ccy = map.get("request_ccy").toString();
                } catch (Exception e) {
                }
                try {
                    this.request_amount = map.get("request_amount").toString();
                } catch (Exception e) {
                }
                try {
                    this.request_mid = map.get("request_mid").toString();
                } catch (Exception e) {
                }
                try {
                    this.payer_name = map.get("payer_name").toString();
                } catch (Exception e) {
                }
                try {
                    this.exp_date = map.get("exp_date").toString();
                } catch (Exception e) {
                }
                try {
                    this.transaction_id = map.get("transaction_id").toString();
                } catch (Exception e) {
                }
                try {
                    this.order_id = map.get("order_id").toString();
                } catch (Exception e) {
                }
                try {
                    this.response_code = map.get("response_code").toString();
                } catch (Exception e) {
                }
                try {
                    this.response_msg = map.get("response_msg").toString();
                } catch (Exception e) {
                }
                try {
                    this.acquirer_response_code = map.get("acquirer_response_code").toString();
                } catch (Exception e) {
                }
                try {
                    this.acquirer_response_msg = map.get("acquirer_response_msg").toString();
                } catch (Exception e) {
                }
                try {
                    this.created_timestamp = map.get("created_timestamp").toString();
                } catch (Exception e) {
                }
                try {
                    this.payer_id = map.get("payer_id").toString();
                } catch (Exception e) {
                }
                try {
                    this.transaction_type = map.get("transaction_type").toString();
                } catch (Exception e) {
                }
                try {
                    this.signature = map.get("signature").toString();
                } catch (Exception e) {
                }
            }

        } catch (Exception ex) {
            try {
                this.mid = map.get("mid").toString();
            } catch (Exception e) {
            }
            try {
                this.card_number = map.get("card_number").toString();
            } catch (Exception e) {
            }
            try {
                this.acquirer_authorized_amount = map.get("acquirer_authorized_amount").toString();
            } catch (Exception e) {
            }
            try {
                this.acquirer_authorized_ccy = map.get("acquirer_authorized_ccy").toString();
            } catch (Exception e) {
            }
            try {
                this.acquirer_created_timestamp = map.get("acquirer_created_timestamp").toString();
            } catch (Exception e) {
            }
            try {
                this.acquirer_transaction_id = map.get("acquirer_transaction_id").toString();
            } catch (Exception e) {
            }
            try {
                this.acquirer_authorization_code = map.get("acquirer_authorization_code").toString();
            } catch (Exception e) {
            }
            try {
                this.last_4 = map.get("last_4").toString();
            } catch (Exception e) {
            }
            try {
                this.first_6 = map.get("first_6").toString();
            } catch (Exception e) {
            }
            try {
                this.payment_mode = map.get("payment_mode").toString();
            } catch (Exception e) {
            }
            try {
                this.merchant_reference = map.get("merchant_reference").toString();
            } catch (Exception e) {
            }
            try {
                this.authorized_ccy = map.get("authorized_ccy").toString();
            } catch (Exception e) {
            }
            try {
                this.request_timestamp = map.get("request_timestamp").toString();
            } catch (Exception e) {
            }
            try {
                this.authorized_amount = map.get("authorized_amount").toString();
            } catch (Exception e) {
            }
            try {
                this.request_ccy = map.get("request_ccy").toString();
            } catch (Exception e) {
            }
            try {
                this.request_amount = map.get("request_amount").toString();
            } catch (Exception e) {
            }
            try {
                this.request_mid = map.get("request_mid").toString();
            } catch (Exception e) {
            }
            try {
                this.payer_name = map.get("payer_name").toString();
            } catch (Exception e) {
            }
            try {
                this.exp_date = map.get("exp_date").toString();
            } catch (Exception e) {
            }
            try {
                this.transaction_id = map.get("transaction_id").toString();
            } catch (Exception e) {
            }
            try {
                this.order_id = map.get("order_id").toString();
            } catch (Exception e) {
            }
            try {
                this.response_code = map.get("response_code").toString();
            } catch (Exception e) {
            }
            try {
                this.response_msg = map.get("response_msg").toString();
            } catch (Exception e) {
            }
            try {
                this.acquirer_response_code = map.get("acquirer_response_code").toString();
            } catch (Exception e) {
            }
            try {
                this.acquirer_response_msg = map.get("acquirer_response_msg").toString();
            } catch (Exception e) {
            }
            try {
                this.created_timestamp = map.get("created_timestamp").toString();
            } catch (Exception e) {
            }
            try {
                this.payer_id = map.get("payer_id").toString();
            } catch (Exception e) {
            }
            try {
                this.transaction_type = map.get("transaction_type").toString();
            } catch (Exception e) {
            }
            try {
                this.signature = map.get("signature").toString();
            } catch (Exception e) {
            }
        }
    }

    /**
     *
     * @return payment_mode [CONDITIONAL] The payment mode or card type that
     * customer has used for the transaction. Please refer to class
     * PaymentChannel (Payment Mode List).
     */
    public String getPaymentMode() {
        return payment_mode;
    }

    /**
     *
     * @return acquirer_authorized_amount [CONDITIONAL] The amount authorized by
     * acquirer e.g. can be different if Acquirer provides DCC feature.
     */
    public String getAcquirerAuthorizedAmount() {
        return acquirer_authorized_amount;
    }

    /**
     *
     * @return acquirer_authorized_ccy [CONDITIONAL] The currency authorized by
     * Acquirer. e.g. can be different if Acquirer provides DCC feature.
     */
    public String getAcquirerAuthorizedCcy() {
        return acquirer_authorized_ccy;
    }

    /**
     *
     * @return merchant_reference [CONDITIONAL] The echo back of
     * merchant_reference in the request.
     */
    public String getMerchantReference() {
        return merchant_reference;
    }

    /**
     *
     * @return authorized_amount [CONDITIONAL] Amount after applying all of
     * others RDP features. E.g. InstanPromo, Currency-Converter, etc.
     */
    public String getAuthorizedAmount() {
        return authorized_amount;
    }

    /**
     *
     * @return authorized_ccy [CONDITIONAL] The final currency that is going to
     * be communicated to Bank/Acquirer. e.g: Due to Currency Converter
     * features.
     */
    public String getAuthorizedCcy() {
        return authorized_ccy;
    }

    /**
     *
     * @return request_mid [CONDITIONAL] The merchant id generated by RDP for
     * merchant, which is used when requesting the payment.
     */
    public String getRequestMid() {
        return request_mid;
    }

    /**
     *
     * @return request_amount [CONDITIONAL] Echo back the amount as is sent in
     * the request.
     */
    public String getRequestAmount() {
        return request_amount;
    }

    /**
     *
     * @return request_ccy [CONDITIONAL] Echo back the ccy as is sent in the
     * request.
     */
    public String getRequestCcy() {
        return request_ccy;
    }

    /**
     *
     * @return payer_id [CONDITIONAL] Merchant defined payer ID or customer ID.
     * Used to identify a unique merchant's customer.
     */
    public String getPayerId() {
        return payer_id;
    }

    /**
     *
     * @return signature [CONDITIONAL] The response signature.
     */
    public String getSignature() {
        return signature;
    }

    /**
     *
     * @return response_code [MANDATORY] Flag which defines whether the
     * transaction is accepted, or has an error in request, or rejected by
     * Acquirer (e.g. Bank) 0: success – accepted transaction -1: bank /
     * acquirer rejection others: request-error
     */
    public String getResponseCode() {
        return response_code;
    }

    /**
     *
     * @return response_msg [MANDATORY] Description on the response-code
     */
    public String getResponseMsg() {
        return response_msg;
    }

    /**
     *
     * @return mid [CONDITIONAL] The merchant ID generated by RDP for merchant,
     * which is used to handle the transaction (can be different from mid used
     * for requesting payment, especially when Merchant has multiple
     * payment-mode with RDP gateway)
     */
    public String getMid() {
        return mid;
    }

    /**
     *
     * @return order_id [CONDITIONAL] An echo back to Merchant’s order-id for
     * the transaction as the identifier of the transaction.
     */
    public String getOrderId() {
        return order_id;
    }

    /**
     *
     * @return transaction_id [CONDITIONAL] The RDP generated unique
     * transaction-id, which is used heavily for identifying the resulted
     * transaction in RDP system.
     */
    public String getTransactionId() {
        return transaction_id;
    }

    /**
     *
     * @return transaction_type [CONDITIONAL] [POSSIBLE VALUE] S : Sale
     * transaction A : Authorization transaction
     */
    public String getTransactionType() {
        return transaction_type;
    }

    /**
     *
     * @return created_timestamp [CONDITIONAL] The date-time when the response
     * is created. In a 24 hour format. Timezone is using (UTC+08:00) Kuala
     * Lumpur, Singapore.
     */
    public String getCreatedTimestamp() {
        return created_timestamp;
    }

    /**
     *
     * @return acquirer_response_code [CONDITIONAL] Response code from acquirer.
     * Format is specific to each Acquirer.
     */
    public String getAcquirerResponseCode() {
        return acquirer_response_code;
    }

    /**
     *
     * @return acquirer_response_msg [CONDITIONAL] Description of the response
     * code.
     */
    public String getAcquirerResponseMsg() {
        return acquirer_response_msg;
    }

    /**
     *
     * @return acquirer_created_timestamp [OPTIONAL] The date-time when the
     * response is created. In a 24 hour format. Timezone vary depends on
     * Acquirer. e.g. 2015-11-14 12:33:27
     */
    public String getAcquirerCreatedTimestamp() {
        return acquirer_created_timestamp;
    }

    /**
     *
     * @return request_timestamp [OPTIONAL] The date-time when the request is
     * created. In a 24 hour format. Timezone vary depends on Acquirer. e.g.
     * 2015-11-14 12:33:27
     */
    public String getRequestTimestamp() {
        return request_timestamp;
    }

    /**
     *
     * @return acquirer_transaction_id [OPTIONAL] Transaction ID generated by
     * Acquirer. Existence depends on availability of the fields from acquirer
     */
    public String getAcquirerTransactionId() {
        return acquirer_transaction_id;
    }

    /**
     *
     * @return acquirer_authorization_code [CONDITIONAL] authorization code from
     * the Bank. Only when it is available from the Bank response.
     */
    public String getAcquirerAuthorizationCode() {
        return acquirer_authorization_code;
    }

    /**
     *
     * @return first_6 [OPTIONAL] The first 6 digits of card number
     */
    public String getFirst6() {
        return first_6;
    }

    /**
     *
     * @return last_4 [OPTIONAL] The last 4 digits of card number
     */
    public String getLast4() {
        return last_4;
    }

    /**
     *
     * @return payer_name [OPTIONAL] The name of card-holder
     */
    public String getPayerName() {
        return payer_name;
    }

    /**
     *
     * @return exp_date [OPTIONAL] The expiry date of the card used for
     * transaction
     */
    public String getExpDate() {
        return exp_date;
    }

    /**
     *
     * @return card_number [OPTIONAL] The card number used by Card IO Scanner
     */
    public String getCardNumber() {
        return card_number;
    }

}
