/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.payment.reddot.library_rdp.main;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import javax.net.ssl.SSLSocket;
import org.apache.commons.httpclient.params.HttpConnectionParams;
import org.apache.commons.httpclient.protocol.ProtocolSocketFactory;
import org.apache.commons.httpclient.protocol.SecureProtocolSocketFactory;

/**
 *
 * @author Red Dot Payment
 */
public class SSLSocketFactoryDev implements SecureProtocolSocketFactory {

    private final SecureProtocolSocketFactory base;

    public SSLSocketFactoryDev(ProtocolSocketFactory base) {
        if (base == null || !(base instanceof SecureProtocolSocketFactory)) {
            throw new IllegalArgumentException();
        }
        this.base = (SecureProtocolSocketFactory) base;
    }

    private Socket stripSSLv3(Socket socket) {
        if (!(socket instanceof SSLSocket)) {
            return socket;
        }
        SSLSocket sslSocket = (SSLSocket) socket;
        List<String> list = new ArrayList<String>();
        for (String s : sslSocket.getSupportedProtocols()) {
            if (!s.startsWith("SSLv2") && !s.startsWith("SSLv3")) {
                list.add(s);
            }
        }
        sslSocket.setEnabledProtocols(list.toArray(new String[list.size()]));
        return sslSocket;
    }

    @Override
    public Socket createSocket(String host, int port) throws IOException {
        return stripSSLv3(base.createSocket(host, port));
    }

    @Override
    public Socket createSocket(
            String host, int port,
            InetAddress localAddress, int localPort) throws IOException {
        return stripSSLv3(base.createSocket(
                host, port,
                localAddress, localPort));
    }

    @Override
    public Socket createSocket(
            String host, int port,
            InetAddress localAddress, int localPort,
            HttpConnectionParams params) throws IOException {
        return stripSSLv3(base.createSocket(
                host, port,
                localAddress, localPort,
                params));
    }

    @Override
    public Socket createSocket(
            Socket socket,
            String host, int port,
            boolean autoClose) throws IOException {
        return stripSSLv3(base.createSocket(
                socket,
                host, port,
                autoClose));
    }
}