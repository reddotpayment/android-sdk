package com.payment.reddot.library_rdp.main;

import android.os.AsyncTask;
import android.util.Log;

import com.payment.reddot.library_rdp.CustomSSLSocketFactory;

import org.apache.commons.httpclient.protocol.Protocol;
import org.apache.commons.httpclient.protocol.ProtocolSocketFactory;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

/**
 * @author Nabler
 * @date 4/10/2017
 */
public class DataProcessor extends AsyncTask<String, Void, String> {

    private String jsonData = "";
    private String urlRequest = "";
    private String jsonResponse = null;

    public DataProcessor(String jsonData, String urlRequest) {
        super();
        this.jsonData = jsonData;
        Log.e("data", jsonData);
        this.urlRequest = urlRequest;

    }


    @Override
    public String doInBackground(String... params) {
        HttpURLConnection urlConnection;
        BufferedReader reader;


        final X509TrustManager trustAllCert = new X509TrustManager() {
            @Override
            public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
            }

            @Override
            public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
            }

            @Override
            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                return new java.security.cert.X509Certificate[]{};
            }
        };

        CustomSSLSocketFactory customSSLSocketFactory = new CustomSSLSocketFactory(trustAllCert);
        HttpsURLConnection.setDefaultSSLSocketFactory(customSSLSocketFactory);

        SSLUtilities.trustAllHostnames();
        SSLUtilities.trustAllHttpsCertificates();


        // Create all-trusting host name verifier
        HostnameVerifier allHostsValid = new HostnameVerifier() {
            @Override
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        };

        // Install the all-trusting host verifier
        HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
        String scheme = "https";
        Protocol baseHttps = Protocol.getProtocol(scheme);
        int defaultPort = baseHttps.getDefaultPort();
        ProtocolSocketFactory baseFactory = baseHttps.getSocketFactory();
        ProtocolSocketFactory customFactory = new SSLSocketFactoryDev(baseFactory);
        Protocol customHttps = new Protocol(scheme, customFactory, defaultPort);
        Protocol.registerProtocol(scheme, customHttps);

        try {
            URL url = new URL(urlRequest);
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setDoOutput(true);
            urlConnection.setRequestMethod("POST");
            urlConnection.setRequestProperty("Content-Type", "application/json");
            urlConnection.setRequestProperty("Accept", "application/json");
            Writer writer = new BufferedWriter(new OutputStreamWriter(urlConnection.getOutputStream(), "UTF-8"));
            writer.write(jsonData);
            writer.close();
            InputStream inputStream = urlConnection.getInputStream();
            StringBuilder buffer = new StringBuilder();
            if (inputStream == null) {
                return null;
            }
            reader = new BufferedReader(new InputStreamReader(inputStream));
            String inputLine;
            while ((inputLine = reader.readLine()) != null) {
                buffer.append(inputLine).append("\n");
            }
            if (buffer.length() == 0) {
                return null;
            }
            jsonResponse = buffer.toString();

            try {
                reader.close();
            } catch (final IOException e) {
                e.printStackTrace();
            }

            urlConnection.disconnect();

            return jsonResponse;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonResponse;
    }
}
