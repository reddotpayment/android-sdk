package com.payment.reddot.library_rdp.main;

import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.payment.reddot.library_rdp.entity.DirectApiRequest;
import com.payment.reddot.library_rdp.entity.TokenizationRequest;
import com.payment.reddot.library_rdp.entity.RedirectApiRequest;

import org.codehaus.jackson.map.ObjectMapper;
import org.json.simple.JSONObject;

import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

/**
 * @author yb
 * @date 2018/3/19
 */
public class GenerateRequestJson {
    private Verificator verificator = new Verificator();
    private static volatile GenerateRequestJson ourInstance = null;

    public static GenerateRequestJson getInstance() {
        if (ourInstance == null) {
            synchronized (GenerateRequestJson.class) {
                if (ourInstance == null) {
                    ourInstance = new GenerateRequestJson();
                }
            }
        }
        return ourInstance;
    }

    private GenerateRequestJson() {
    }


    public String directPayment(DirectApiRequest request, String secretKey) throws IOException {
        Gson gson = new Gson();
        StringBuilder calculateString;
        if (!TextUtils.isEmpty(request.getCard_no())) {
            calculateString = cardNoRequest(request);
        } else if (!TextUtils.isEmpty(request.getPayer_id())) {
            calculateString = payerId(request);
        } else {
            calculateString = tokenIdRequest(request);
        }
        calculateString.append(secretKey);
        Log.e("data", calculateString.toString());
        JsonElement jsonElement = gson.toJsonTree(request);
        jsonElement.getAsJsonObject().addProperty("signature", verificator.generateHash(calculateString.toString()));
//        DataProcessor processData = new DataProcessor(jsonAfterSignature, urlRequestPayment);
//        try {
//            return processData.execute().get();
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        } catch (ExecutionException e) {
//            e.printStackTrace();
//        }
        return gson.toJson(jsonElement);

    }

    public String redirectPayment(RedirectApiRequest request, String secretKey) {
        Gson gson = new Gson();
        StringBuilder calculateString = new StringBuilder();
        calculateString.append(request.getMid());
        calculateString.append(request.getOrder_id());
        calculateString.append(request.getPayment_type());
        calculateString.append(request.getAmount());
        calculateString.append(request.getCcy());
        String cardNo = request.getCard_no();
        if (!TextUtils.isEmpty(cardNo)) {
            calculateString.append(cardNo.substring(0, 6));
            calculateString.append(cardNo.substring(cardNo.length() - 4, cardNo.length()));
            if (!TextUtils.isEmpty(String.valueOf(request.getExp_date()))) {
                calculateString.append(request.getExp_date());
            }
            String cvv2 = request.getCvv2();
            if (!TextUtils.isEmpty(cvv2)) {
                calculateString.append(cvv2.substring(cvv2.length() - 1, cvv2.length()));
            }
        } else {
            if (!TextUtils.isEmpty(request.getPayer_id())) {
                calculateString.append(request.getPayer_id());
            }
            String cvv2 = request.getCvv2();

            if (!TextUtils.isEmpty(request.getCvv2())) {
                calculateString.append(cvv2.substring(cvv2.length() - 1, cvv2.length()));
            }
        }

        calculateString.append(secretKey);

        JsonElement jsonElement = gson.toJsonTree(request);
        jsonElement.getAsJsonObject().addProperty("signature", verificator.generateHash(calculateString.toString()));
        return gson.toJson(jsonElement);
    }


    public String getQueryPaymentResult(String transactionId, String mid, String secretKey) {

        StringBuilder calculateString = new StringBuilder();
        JSONObject postParameters = new JSONObject();
        postParameters.put("transaction_id", transactionId);
        postParameters.put("request_mid", mid);
        TreeMap<String, String> calculateSignature = new TreeMap<>(postParameters);
        for (Map.Entry<String, String> treeMap : calculateSignature.entrySet()) {
            calculateString.append(treeMap.getValue());
        }
        calculateString.append(secretKey);
        postParameters.put("signature", verificator.generateHash(calculateString.toString()));

        return postParameters.toString();
    }

    public String hostedTokenApi(TokenizationRequest request, String secretKey) throws IOException {
        Gson gson = new Gson();
        StringBuilder calculateString = new StringBuilder();
        String jsonBeforeSignature = gson.toJson(request);
        ObjectMapper objectMapper = new ObjectMapper();
        TreeMap<String, String> sortedMap = objectMapper.readValue(jsonBeforeSignature, TreeMap.class);
        for (Map.Entry<String, String> treeMap : sortedMap.entrySet()) {
            if (!TextUtils.isEmpty(treeMap.getValue()) && !"null".equals(treeMap.getValue())) {
                calculateString.append(treeMap.getValue());
            }
        }
        calculateString.append(secretKey);
        JsonElement jsonElement = gson.toJsonTree(request);
        jsonElement.getAsJsonObject().addProperty("signature", verificator.generateHash(calculateString.toString()));


        return gson.toJson(jsonElement);
    }


    public String directTokenizationApi(TokenizationRequest request, String secretKey) throws IOException {
        Gson gson = new Gson();
        StringBuilder calculateString = new StringBuilder();
        String jsonBeforeSignature = gson.toJson(request);
        ObjectMapper objectMapper = new ObjectMapper();
        TreeMap<String, String> sortedMap = objectMapper.readValue(jsonBeforeSignature, TreeMap.class);
        for (Map.Entry<String, String> treeMap : sortedMap.entrySet()) {
            if (!TextUtils.isEmpty(treeMap.getValue()) && !"null".equals(treeMap.getValue())) {
                calculateString.append(treeMap.getValue());
            }
        }
        calculateString.append(secretKey);
        JsonElement jsonElement = gson.toJsonTree(request);
        jsonElement.getAsJsonObject().addProperty("signature", verificator.generateHash(calculateString.toString()));

        return gson.toJson(jsonElement);
    }

    private StringBuilder cardNoRequest(DirectApiRequest request) {

        StringBuilder calculateString = new StringBuilder();
        calculateString.append(request.getMid());
        calculateString.append(request.getOrder_id());
        calculateString.append(request.getPayment_type());
        calculateString.append(request.getAmount());
        calculateString.append(request.getCcy());
        String cardNo = request.getCard_no();
        calculateString.append(cardNo.substring(0, 6));
        calculateString.append(cardNo.substring(cardNo.length() - 4, cardNo.length()));
        if (!TextUtils.isEmpty(request.getExp_date())) {
            calculateString.append(request.getExp_date());
        }
        String cvv2 = request.getCvv2();
        if (!TextUtils.isEmpty(cvv2)) {
            calculateString.append(cvv2.substring(cvv2.length() - 1, cvv2.length()));
        }
        return calculateString;
    }

    private StringBuilder tokenIdRequest(DirectApiRequest request) {
        StringBuilder calculateString = new StringBuilder();
        calculateString.append(request.getMid());
        calculateString.append(request.getOrder_id());
        calculateString.append(request.getPayment_type());
        calculateString.append(request.getAmount());
        calculateString.append(request.getCcy());
        String tokenId = request.getToken_id();
        if (!TextUtils.isEmpty(tokenId)) {
            calculateString.append(tokenId.substring(0, 6));
            calculateString.append(tokenId.substring(tokenId.length() - 4, tokenId.length()));
            if (!TextUtils.isEmpty(request.getExp_date())) {
                calculateString.append(request.getExp_date());
            }
        }
        String cvv2 = request.getCvv2();
        if (!TextUtils.isEmpty(cvv2)) {
            calculateString.append(cvv2.substring(cvv2.length() - 1, cvv2.length()));
        }
        return calculateString;
    }

    private StringBuilder payerId(DirectApiRequest request) {
        StringBuilder calculateString = new StringBuilder();
        calculateString.append(request.getMid());
        calculateString.append(request.getOrder_id());
        calculateString.append(request.getPayment_type());
        calculateString.append(request.getAmount());
        calculateString.append(request.getCcy());
        calculateString.append(request.getPayer_id());
        if (!TextUtils.isEmpty(request.getExp_date())) {
            calculateString.append(request.getExp_date());
        }
        String cvv2 = request.getCvv2();
        if (!TextUtils.isEmpty(cvv2)) {
            calculateString.append(cvv2.substring(cvv2.length() - 1, cvv2.length()));
        }
        return calculateString;
    }
}
