/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.payment.reddot.library_rdp.main;

/**
 *
 * @author Red Dot Payment
 */
public enum ApiMode {
    DIRECT_TOKEN_API {
      @Override
      public String toString() {
          return "direct_token_api";
      }
    },
    HOSTED_TOKEN_API {
      @Override
      public String toString() {
          return "hosted_token_api";
      }
    },
    DIRECT_N3D {
        @Override
        public String toString() {
            return "direct_n3d";
        }
    },
    REDIRECTION_SOP {
        @Override
        public String toString() {
            return "redirection_sop";
        }
    },
    REDIRECTION_HOSTED {
      @Override
      public String toString() {
          return "redirection_hosted";
      }
    };
}
