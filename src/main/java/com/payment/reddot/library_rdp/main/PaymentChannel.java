/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.payment.reddot.library_rdp.main;

/**
 *
 * @author Red Dot Payment
 */
public class PaymentChannel {

    public final static int VISA_OR_MASTERCARD = 1;
    public final static int AMEX = 2;
    public final static int UPOP = 3;
    public final static int ALIPAY = 4;
    public final static int TENPAY = 5;
    public final static int NINETY_NINE_BILL = 6;
    public final static int ENETS = 7;
    public final static int PAYLAH = 8;

}
