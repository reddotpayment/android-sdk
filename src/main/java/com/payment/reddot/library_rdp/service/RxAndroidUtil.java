package com.payment.reddot.library_rdp.service;

import rx.Observable;
import rx.functions.Action0;
import rx.functions.Action1;
import rx.schedulers.Schedulers;


/**
 * @author yb
 * @date 2018/3/8
 */
public class RxAndroidUtil {

    private final static Observable.Transformer<Observable, Observable> schedulersTransformer =
            observable -> observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidScheduler.mainThread());

    /**
     * Add subscribe scheduler and observe scheduler for observable
     *
     * @see <a href="http://blog.danlew.net/2015/03/02/dont-break-the-chain/">Don't break the chain</a>
     */
    @SuppressWarnings("unchecked")
    public static <T> Observable.Transformer<T, T> applySchedulers() {
        return (Observable.Transformer<T, T>) schedulersTransformer;
    }

    public static <T> void subscribe(Observable<T> observable,
                                     Action1<T> onNext,
                                     Action1<Throwable> onError) {

        observable.compose(applySchedulers())
                .onBackpressureDrop()
                .subscribe(onNext, onError);
    }

    public static <T> void subscribe(Observable<T> observable,
                                     Action1<T> onNext,
                                     Action1<Throwable> onError,
                                     Action0 onComplete) {
        observable.compose(applySchedulers())
                .onBackpressureDrop()
                .subscribe(onNext, onError, onComplete);
    }


}
