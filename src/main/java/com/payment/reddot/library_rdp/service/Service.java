package com.payment.reddot.library_rdp.service;

import retrofit2.http.Body;
import retrofit2.http.POST;
import rx.Observable;

/**
 * @author yb
 * @date 2018/3/8
 */
public interface Service {

    @POST("token-api")
    Observable<String> hostTokenApi(@Body String request);

    @POST("token-api")
    Observable<String> directTokenApi(@Body String request);

    @POST("Merchant_processor/query_token_redirection")
    Observable<String> query_token_redirection(@Body String request);

    @POST("payment-api")
    Observable<String> paymentApi(@Body String request);

    @POST("Merchant_processor/query_redirection")
    Observable<String> queryRedirection(@Body String request);

}
