package com.payment.reddot.library_rdp.service;

import com.payment.reddot.library_rdp.CustomSSLSocketFactory;
import com.payment.reddot.library_rdp.main.Constant;

import java.security.cert.CertificateException;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.X509TrustManager;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

/**
 * @author yb
 * @date 2018/3/8
 */
public class ServiceManager {
    private String baseUrl;

    private Service service;
    private static volatile ServiceManager ourInstance = null;

    public static ServiceManager getInstance() {
        if (ourInstance == null) {
            synchronized (ServiceManager.class) {
                if (ourInstance == null) {
                    ourInstance = new ServiceManager();
                }
            }
        }
        return ourInstance;
    }

    private ServiceManager() {
    }

    public void init(int environment) {
        if (environment == Constant.ENVIRONMENT_LIVE) {
            baseUrl = "https://secure.reddotpayment.com/service/";
        } else {
            baseUrl = "https://secure-dev.reddotpayment.com/service/";
        }
        service = buildService();
    }

    private Service buildService() {

        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        final X509TrustManager trustAllCert = new X509TrustManager() {
            @Override
            public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
            }

            @Override
            public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
            }

            @Override
            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                return new java.security.cert.X509Certificate[]{};
            }
        };
        OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .addInterceptor(loggingInterceptor)
                .sslSocketFactory(new CustomSSLSocketFactory(trustAllCert), trustAllCert);

        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(clientBuilder.build())
                .addConverterFactory(ScalarsConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build()
                .create(Service.class);
    }

    public Service getService() {
        return service;
    }
}
