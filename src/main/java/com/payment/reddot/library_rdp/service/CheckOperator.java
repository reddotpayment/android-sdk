package com.payment.reddot.library_rdp.service;

import android.accounts.NetworkErrorException;


import java.net.SocketTimeoutException;

import rx.Observable;
import rx.Subscriber;

/**
 * @author yb
 * @date 2018/3/6
 */
public class CheckOperator<T> implements Observable.Operator<T, T> {
    @Override
    public Subscriber<? super T> call(Subscriber<? super T> subscriber) {
        return new Subscriber<T>() {

            @Override
            public void onCompleted() {
                subscriber.onCompleted();
            }

            @Override
            public void onError(Throwable e) {
                if (!subscriber.isUnsubscribed()) {
                    subscriber.onError(e);
                }
            }

            @Override
            public void onNext(T t) {
                if (!subscriber.isUnsubscribed()) {
                    if (t instanceof Throwable) {
                        if (t instanceof NetworkErrorException) {
                            subscriber.onError(new NetworkErrorException());
                        } else if (t instanceof SocketTimeoutException) {
                            subscriber.onError(new SocketTimeoutException());
                        } else {
                            subscriber.onError((Throwable) (t));
                        }
                    } else {
                        subscriber.onNext(t);
                    }
                }
            }
        };
    }
}
