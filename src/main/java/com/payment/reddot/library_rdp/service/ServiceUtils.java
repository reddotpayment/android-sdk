package com.payment.reddot.library_rdp.service;

import rx.Observable;
import rx.functions.Action1;

/**
 * @author yb
 * @date 2018/3/12
 */
public class ServiceUtils {


    protected Service getService() {
        return ServiceManager.getInstance().getService();
    }

    public static <T> void subscribe(Observable<T> observable, Action1<T> onNext, Action1<Throwable> onError) {
        RxAndroidUtil.subscribe(toCheckoutLogoutObservable(observable), onNext, onError);
    }

    private static <T> Observable<T> toCheckoutLogoutObservable(Observable<T> observable) {
        return observable.lift(new CheckOperator<>());
    }
}
