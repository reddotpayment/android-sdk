package com.payment.reddot.library_rdp.entity;

/**
 * @author yb
 * @date 2018/3/6
 */
public class DirectApiRequest {

    /**
     * mid : 1007778080
     * amount : 1.00
     * payment_type : S
     * card_no : 4005550000000001
     * ccy : SGD
     * order_id : 1520327863
     * api_mode : direct_n3d
     * signature : 8082822c119f9010173bc76d24c6b954aaff381e9a9bfdfa5efe24eb1be8d0cd61b3149992d5c67db7ff5ef88bb445f57cdc60e722c8e94fcd37feca56068689
     * exp_date : 122020
     * payer_name : Test Payer
     * payer_email : 54511@qq.com
     * cvv2 : 123
     */
    private String bill_to_forename = null;
    private String bill_to_surname = null;
    private String bill_to_address_city = null;
    private String bill_to_address_line1 = null;
    private String bill_to_address_country = null;
    private String bill_to_address_state = null;
    private String bill_to_address_postal_code = null;
    private String bill_to_phone = null;
    private String mid;
    private String amount;
    private String payment_type;
    private String card_no;
    private String ccy;
    private String order_id;
    private String signature;
    private String exp_date;
    private String payer_name;
    private String payer_id;
    private String payer_email;
    private String cvv2;
    private String token_id;
    private String token_mod;
    private String merchant_reference;
    private String secretKey;
    private String client_user_agent;
    private String client_ip_address;
    private String tenor_month;
    private String api_mode;
    private String token_mod_id;
    private String notify_url;
    private String bill_to_address_line2;


    public String getNotify_url() {
        return notify_url;
    }

    public void setNotify_url(String notify_url) {
        this.notify_url = notify_url;
    }

    public String getBill_to_address_line2() {
        return bill_to_address_line2;
    }

    public void setBill_to_address_line2(String bill_to_address_line2) {
        this.bill_to_address_line2 = bill_to_address_line2;
    }

    public String getToken_mod_id() {
        return token_mod_id;
    }

    public void setToken_mod_id(String token_mod_id) {
        this.token_mod_id = token_mod_id;
    }

    public String getMerchant_reference() {
        return merchant_reference;
    }

    public void setMerchant_reference(String merchant_reference) {
        this.merchant_reference = merchant_reference;
    }

    public String getBill_to_forename() {
        return bill_to_forename;
    }

    public void setBill_to_forename(String bill_to_forename) {
        this.bill_to_forename = bill_to_forename;
    }

    public String getBill_to_surname() {
        return bill_to_surname;
    }

    public void setBill_to_surname(String bill_to_surname) {
        this.bill_to_surname = bill_to_surname;
    }

    public String getBill_to_address_city() {
        return bill_to_address_city;
    }

    public void setBill_to_address_city(String bill_to_address_city) {
        this.bill_to_address_city = bill_to_address_city;
    }

    public String getBill_to_address_line1() {
        return bill_to_address_line1;
    }

    public void setBill_to_address_line1(String bill_to_address_line1) {
        this.bill_to_address_line1 = bill_to_address_line1;
    }

    public String getBill_to_address_country() {
        return bill_to_address_country;
    }

    public void setBill_to_address_country(String bill_to_address_country) {
        this.bill_to_address_country = bill_to_address_country;
    }

    public String getBill_to_address_state() {
        return bill_to_address_state;
    }

    public void setBill_to_address_state(String bill_to_address_state) {
        this.bill_to_address_state = bill_to_address_state;
    }

    public String getBill_to_address_postal_code() {
        return bill_to_address_postal_code;
    }

    public void setBill_to_address_postal_code(String bill_to_address_postal_code) {
        this.bill_to_address_postal_code = bill_to_address_postal_code;
    }

    public String getBill_to_phone() {
        return bill_to_phone;
    }

    public void setBill_to_phone(String bill_to_phone) {
        this.bill_to_phone = bill_to_phone;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public String getClient_user_agent() {
        return client_user_agent;
    }

    public void setClient_user_agent(String client_user_agent) {
        this.client_user_agent = client_user_agent;
    }

    public String getClient_ip_address() {
        return client_ip_address;
    }

    public void setClient_ip_address(String client_ip_address) {
        this.client_ip_address = client_ip_address;
    }

    public String getTenor_month() {
        return tenor_month;
    }

    public void setTenor_month(String tenor_month) {
        this.tenor_month = tenor_month;
    }

    public String getToken_mod() {
        return token_mod;
    }

    public void setToken_mod(String token_mod) {
        this.token_mod = token_mod;
    }

    public String getToken_id() {
        return token_id;
    }

    public void setToken_id(String token_id) {
        this.token_id = token_id;
    }

    public String getPayer_id() {
        return payer_id;
    }

    public void setPayer_id(String payer_id) {
        this.payer_id = payer_id;
    }

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getPayment_type() {
        return payment_type;
    }

    public void setPayment_type(String payment_type) {
        this.payment_type = payment_type;
    }

    public String getCard_no() {
        return card_no;
    }

    public void setCard_no(String card_no) {
        this.card_no = card_no;
    }

    public String getCcy() {
        return ccy;
    }

    public void setCcy(String ccy) {
        this.ccy = ccy;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getApi_mode() {
        return api_mode;
    }

    public void setApi_mode(String api_mode) {
        this.api_mode = api_mode;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getExp_date() {
        return exp_date;
    }

    public void setExp_date(String exp_date) {
        this.exp_date = exp_date;
    }

    public String getPayer_name() {
        return payer_name;
    }

    public void setPayer_name(String payer_name) {
        this.payer_name = payer_name;
    }

    public String getPayer_email() {
        return payer_email;
    }

    public void setPayer_email(String payer_email) {
        this.payer_email = payer_email;
    }

    public String getCvv2() {
        return cvv2;
    }

    public void setCvv2(String cvv2) {
        this.cvv2 = cvv2;
    }
}
