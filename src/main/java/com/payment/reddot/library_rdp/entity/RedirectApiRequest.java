package com.payment.reddot.library_rdp.entity;

/**
 * @author yb
 * @date 2018/3/7
 */
public class RedirectApiRequest {


    /**
     * mid : 0000021009
     * redirect_url : https://dev.emuzi-tech.com/HippoStart_V3/callback/RDPPaySuccess
     * notify_url : https://dev.emuzi-tech.com/HippoStart_V3/callback/RDBPayNotify
     * bill_to_address_city : bill_to_address_city
     * amount : 1.00
     * payment_type : S
     * ccy : SGD
     * bill_to_address_country : bill_to_address_country
     * order_id : 1520407741
     * card_no : 4111111111111111
     * api_mode : redirection_hosted
     * bill_to_address_line1 : bill_to_address_line1
     * back_url : https://dev.emuzi-tech.com/HippoStart_V3/callback/RDBCancel
     * exp_date : 122022
     * bill_to_address_postal_code : bill_to_address_postal_code
     * payer_name : Test Payer
     * payer_email : 54511@qq.com
     * cvv2 : 123
     * signature : 579eb63e515e024f7fd88314eeaec1fc0622f98d4e042b86e157a4029c0d28bdcce5d8f6420bcefbc65afe3aeed0cff367c623c6da58da6099ec5dc6f219ffc5
     */
    private String mid;
    private String token_id;
    private String token_mod_id;
    private String signature;
    private String back_url;
    private String redirect_url;
    private String notify_url;
    private String order_id;
    private String api_mode;
    private String ccy;
    private String amount;
    private String payment_type;
    private String payer_email;
    private String merchant_reference;
    private String locale;
    private String multiple_method_page;
    private String payer_name;
    private String payer_id;
    private String card_no;
    private String exp_date;
    private String cvv2;
    private String bill_to_surname;
    private String bill_to_forename;
    private String bill_to_address_city;
    private String bill_to_address_line1;
    private String bill_to_address_line2;
    private String bill_to_address_country;
    private String bill_to_address_state;
    private String bill_to_address_postal_code;
    private String bill_to_phone;
    private String ship_to_forename;
    private String ship_to_surname;
    private String ship_to_address_city;
    private String ship_to_address_line1;
    private String ship_to_address_line2;
    private String ship_to_address_country;
    private String ship_to_address_state;
    private String ship_to_address_postal_code;
    private String ship_to_phone;
    private String tenor_month;
    private String token_mod;
    private String payment_channel;
    private String store_code;

    public String getToken_id() {
        return token_id;
    }

    public void setToken_id(String token_id) {
        this.token_id = token_id;
    }

    public String getToken_mod_id() {
        return token_mod_id;
    }

    public void setToken_mod_id(String token_mod_id) {
        this.token_mod_id = token_mod_id;
    }

    public String getMerchant_reference() {
        return merchant_reference;
    }

    public void setMerchant_reference(String merchant_reference) {
        this.merchant_reference = merchant_reference;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public String getMultiple_method_page() {
        return multiple_method_page;
    }

    public void setMultiple_method_page(String multiple_method_page) {
        this.multiple_method_page = multiple_method_page;
    }

    public String getPayer_id() {
        return payer_id;
    }

    public void setPayer_id(String payer_id) {
        this.payer_id = payer_id;
    }

    public String getBill_to_surname() {
        return bill_to_surname;
    }

    public void setBill_to_surname(String bill_to_surname) {
        this.bill_to_surname = bill_to_surname;
    }

    public String getBill_to_forename() {
        return bill_to_forename;
    }

    public void setBill_to_forename(String bill_to_forename) {
        this.bill_to_forename = bill_to_forename;
    }

    public String getBill_to_address_line2() {
        return bill_to_address_line2;
    }

    public void setBill_to_address_line2(String bill_to_address_line2) {
        this.bill_to_address_line2 = bill_to_address_line2;
    }

    public String getBill_to_address_state() {
        return bill_to_address_state;
    }

    public void setBill_to_address_state(String bill_to_address_state) {
        this.bill_to_address_state = bill_to_address_state;
    }

    public String getBill_to_phone() {
        return bill_to_phone;
    }

    public void setBill_to_phone(String bill_to_phone) {
        this.bill_to_phone = bill_to_phone;
    }

    public String getShip_to_forename() {
        return ship_to_forename;
    }

    public void setShip_to_forename(String ship_to_forename) {
        this.ship_to_forename = ship_to_forename;
    }

    public String getShip_to_surname() {
        return ship_to_surname;
    }

    public void setShip_to_surname(String ship_to_surname) {
        this.ship_to_surname = ship_to_surname;
    }

    public String getShip_to_address_city() {
        return ship_to_address_city;
    }

    public void setShip_to_address_city(String ship_to_address_city) {
        this.ship_to_address_city = ship_to_address_city;
    }

    public String getShip_to_address_line1() {
        return ship_to_address_line1;
    }

    public void setShip_to_address_line1(String ship_to_address_line1) {
        this.ship_to_address_line1 = ship_to_address_line1;
    }

    public String getShip_to_address_line2() {
        return ship_to_address_line2;
    }

    public void setShip_to_address_line2(String ship_to_address_line2) {
        this.ship_to_address_line2 = ship_to_address_line2;
    }

    public String getShip_to_address_country() {
        return ship_to_address_country;
    }

    public void setShip_to_address_country(String ship_to_address_country) {
        this.ship_to_address_country = ship_to_address_country;
    }

    public String getShip_to_address_state() {
        return ship_to_address_state;
    }

    public void setShip_to_address_state(String ship_to_address_state) {
        this.ship_to_address_state = ship_to_address_state;
    }

    public String getShip_to_address_postal_code() {
        return ship_to_address_postal_code;
    }

    public void setShip_to_address_postal_code(String ship_to_address_postal_code) {
        this.ship_to_address_postal_code = ship_to_address_postal_code;
    }

    public String getShip_to_phone() {
        return ship_to_phone;
    }

    public void setShip_to_phone(String ship_to_phone) {
        this.ship_to_phone = ship_to_phone;
    }

    public String getTenor_month() {
        return tenor_month;
    }

    public void setTenor_month(String tenor_month) {
        this.tenor_month = tenor_month;
    }

    public String getToken_mod() {
        return token_mod;
    }

    public void setToken_mod(String token_mod) {
        this.token_mod = token_mod;
    }

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    public String getRedirect_url() {
        return redirect_url;
    }

    public void setRedirect_url(String redirect_url) {
        this.redirect_url = redirect_url;
    }

    public String getNotify_url() {
        return notify_url;
    }

    public void setNotify_url(String notify_url) {
        this.notify_url = notify_url;
    }

    public String getBill_to_address_city() {
        return bill_to_address_city;
    }

    public void setBill_to_address_city(String bill_to_address_city) {
        this.bill_to_address_city = bill_to_address_city;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getPayment_type() {
        return payment_type;
    }

    public void setPayment_type(String payment_type) {
        this.payment_type = payment_type;
    }

    public String getCcy() {
        return ccy;
    }

    public void setCcy(String ccy) {
        this.ccy = ccy;
    }

    public String getBill_to_address_country() {
        return bill_to_address_country;
    }

    public void setBill_to_address_country(String bill_to_address_country) {
        this.bill_to_address_country = bill_to_address_country;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getCard_no() {
        return card_no;
    }

    public void setCard_no(String card_no) {
        this.card_no = card_no;
    }

    public String getApi_mode() {
        return api_mode;
    }

    public void setApi_mode(String api_mode) {
        this.api_mode = api_mode;
    }

    public String getBill_to_address_line1() {
        return bill_to_address_line1;
    }

    public void setBill_to_address_line1(String bill_to_address_line1) {
        this.bill_to_address_line1 = bill_to_address_line1;
    }

    public String getBack_url() {
        return back_url;
    }

    public void setBack_url(String back_url) {
        this.back_url = back_url;
    }

    public String getExp_date() {
        return exp_date;
    }

    public void setExp_date(String exp_date) {
        this.exp_date = exp_date;
    }

    public String getBill_to_address_postal_code() {
        return bill_to_address_postal_code;
    }

    public void setBill_to_address_postal_code(String bill_to_address_postal_code) {
        this.bill_to_address_postal_code = bill_to_address_postal_code;
    }

    public String getPayer_name() {
        return payer_name;
    }

    public void setPayer_name(String payer_name) {
        this.payer_name = payer_name;
    }

    public String getPayer_email() {
        return payer_email;
    }

    public void setPayer_email(String payer_email) {
        this.payer_email = payer_email;
    }

    public String getCvv2() {
        return cvv2;
    }

    public void setCvv2(String cvv2) {
        this.cvv2 = cvv2;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }


    public String getPayment_channel() {
        return payment_channel;
    }

    public void setPayment_channel(String payment_channel) {
        this.payment_channel = payment_channel;
    }

    public String getStore_code() {
        return store_code;
    }

    public void setStore_code(String store_code) {
        this.store_code = store_code;
    }
}
