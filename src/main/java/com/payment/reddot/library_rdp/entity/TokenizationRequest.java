package com.payment.reddot.library_rdp.entity;


/**
 * @author yb
 * @date 2018/3/6
 */
public class TokenizationRequest {
    private String mid = null;
    private String order_id = null;
    private String api_mode = null;
    private String transaction_type = null;
    private String type = null;
    private String payer_email = null;
    private String payer_name = null;

    /**
     * Conditional
     */

    private String payer_id = null;
    private String card_no = null;
    private String exp_date = null;
    private String cvv2 = null;
    private String ccy = null;
    private String bill_to_forename = null;
    private String bill_to_surname = null;
    private String bill_to_address_city = null;
    private String bill_to_address_line1 = null;
    private String bill_to_address_country = null;
    private String bill_to_address_state = null;
    private String bill_to_address_postal_code = null;
    private String bill_to_phone = null;
    private String wallet_id = null;
    private String token_id = null;
    //Optional
    private String merchant_reference = null;
    private String bill_to_address_line2 = null;
    private String notify_url = null;
    private String redirect_url = null;
    private String back_url = null;
    private String signature = null;


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCcy() {
        return ccy;
    }

    public void setCcy(String ccy) {
        this.ccy = ccy;
    }

    public String getRedirect_url() {
        return redirect_url;
    }

    public void setRedirect_url(String redirect_url) {
        this.redirect_url = redirect_url;
    }

    public String getBack_url() {
        return back_url;
    }

    public void setBack_url(String back_url) {
        this.back_url = back_url;
    }

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getApi_mode() {
        return api_mode;
    }

    public void setApi_mode(String api_mode) {
        this.api_mode = api_mode;
    }

    public String getTransaction_type() {
        return transaction_type;
    }

    public void setTransaction_type(String transaction_type) {
        this.transaction_type = transaction_type;
    }

    public String getPayer_email() {
        return payer_email;
    }

    public void setPayer_email(String payer_email) {
        this.payer_email = payer_email;
    }

    public String getPayer_name() {
        return payer_name;
    }

    public void setPayer_name(String payer_name) {
        this.payer_name = payer_name;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getPayer_id() {
        return payer_id;
    }

    public void setPayer_id(String payer_id) {
        this.payer_id = payer_id;
    }

    public String getCard_no() {
        return card_no;
    }

    public void setCard_no(String card_no) {
        this.card_no = card_no;
    }

    public String getExp_date() {
        return exp_date;
    }

    public void setExp_date(String exp_date) {
        this.exp_date = exp_date;
    }

    public String getCvv2() {
        return cvv2;
    }

    public void setCvv2(String cvv2) {
        this.cvv2 = cvv2;
    }

    public String getBill_to_forename() {
        return bill_to_forename;
    }

    public void setBill_to_forename(String bill_to_forename) {
        this.bill_to_forename = bill_to_forename;
    }

    public String getBill_to_surname() {
        return bill_to_surname;
    }

    public void setBill_to_surname(String bill_to_surname) {
        this.bill_to_surname = bill_to_surname;
    }

    public String getBill_to_address_city() {
        return bill_to_address_city;
    }

    public void setBill_to_address_city(String bill_to_address_city) {
        this.bill_to_address_city = bill_to_address_city;
    }

    public String getBill_to_address_line1() {
        return bill_to_address_line1;
    }

    public void setBill_to_address_line1(String bill_to_address_line1) {
        this.bill_to_address_line1 = bill_to_address_line1;
    }

    public String getBill_to_address_country() {
        return bill_to_address_country;
    }

    public void setBill_to_address_country(String bill_to_address_country) {
        this.bill_to_address_country = bill_to_address_country;
    }

    public String getBill_to_address_state() {
        return bill_to_address_state;
    }

    public void setBill_to_address_state(String bill_to_address_state) {
        this.bill_to_address_state = bill_to_address_state;
    }

    public String getBill_to_address_postal_code() {
        return bill_to_address_postal_code;
    }

    public void setBill_to_address_postal_code(String bill_to_address_postal_code) {
        this.bill_to_address_postal_code = bill_to_address_postal_code;
    }

    public String getBill_to_phone() {
        return bill_to_phone;
    }

    public void setBill_to_phone(String bill_to_phone) {
        this.bill_to_phone = bill_to_phone;
    }

    public String getWallet_id() {
        return wallet_id;
    }

    public void setWallet_id(String wallet_id) {
        this.wallet_id = wallet_id;
    }

    public String getToken_id() {
        return token_id;
    }

    public void setToken_id(String token_id) {
        this.token_id = token_id;
    }

    public String getMerchant_reference() {
        return merchant_reference;
    }

    public void setMerchant_reference(String merchant_reference) {
        this.merchant_reference = merchant_reference;
    }

    public String getBill_to_address_line2() {
        return bill_to_address_line2;
    }

    public void setBill_to_address_line2(String bill_to_address_line2) {
        this.bill_to_address_line2 = bill_to_address_line2;
    }

    public String getNotify_url() {
        return notify_url;
    }

    public void setNotify_url(String notify_url) {
        this.notify_url = notify_url;
    }
}
