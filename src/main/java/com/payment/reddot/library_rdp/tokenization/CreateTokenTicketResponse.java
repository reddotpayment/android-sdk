/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.payment.reddot.library_rdp.tokenization;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.payment.reddot.library_rdp.main.Verificator;

import java.lang.reflect.Type;
import java.net.URL;
import java.util.Map;
import java.util.TreeMap;

/**
 * @author Red Dot Payment
 */
public class CreateTokenTicketResponse {

    private String mid;
    private String signature;
    private String expired_timestamp;
    private String created_timestamp;
    private String order_id;
    private URL payment_url;
    private String response_msg;
    private String response_code;

    /**
     * @param response   The raw string of JSON response from web service API.
     * @param secret_key The secret-key which is used to generate the signature for later validate
     *                   the signature, in case of wrong signature all members will be null.
     */
    public CreateTokenTicketResponse(String response, String secret_key) {
        StringBuilder calculateString = new StringBuilder();
        Verificator verificator = new Verificator();
        Gson gson = new Gson();
        Type stringStringMap = new TypeToken<Map<String, String>>() {
        }.getType();
        Map<String, String> map = gson.fromJson(response, stringStringMap);
        TreeMap<String, String> sortedParameter = new TreeMap<>(map);
        for (Map.Entry<String, String> treeMap : sortedParameter.entrySet()) {
            if (treeMap.getKey().equals("signature")) {
            } else {
                calculateString.append(treeMap.getValue());
            }
        }
        calculateString.append(secret_key);
        try {
            if (map.get("signature").toString().equals(verificator.generateHash(calculateString.toString()))) {
                try {
                    this.payment_url = new URL(map.get("payment_url").toString());
                } catch (Exception e) {
                }
                try {
                    this.mid = map.get("mid").toString();
                } catch (Exception e) {
                }
                try {
                    this.signature = map.get("signature").toString();
                } catch (Exception e) {
                }
                try {
                    this.expired_timestamp = map.get("expired_timestamp").toString();
                } catch (Exception e) {
                }
                try {
                    this.created_timestamp = map.get("created_timestamp").toString();
                } catch (Exception e) {
                }
                try {
                    this.order_id = map.get("order_id").toString();
                } catch (Exception e) {
                }
                try {
                    this.response_code = map.get("response_code").toString();
                } catch (Exception e) {
                }
                try {
                    this.response_msg = map.get("response_msg").toString();
                } catch (Exception e) {
                }
            }
        } catch (Exception ex) {
            try {
                this.payment_url = new URL(map.get("payment_url").toString());
            } catch (Exception e) {
            }
            try {
                this.mid = map.get("mid").toString();
            } catch (Exception e) {
            }
            try {
                this.signature = map.get("signature").toString();
            } catch (Exception e) {
            }
            try {
                this.expired_timestamp = map.get("expired_timestamp").toString();
            } catch (Exception e) {
            }
            try {
                this.created_timestamp = map.get("created_timestamp").toString();
            } catch (Exception e) {
            }
            try {
                this.order_id = map.get("order_id").toString();
            } catch (Exception e) {
            }
            try {
                this.response_code = map.get("response_code").toString();
            } catch (Exception e) {
            }
            try {
                this.response_msg = map.get("response_msg").toString();
            } catch (Exception e) {
            }
        }
    }

    /**
     * @return [conditional : no-error][VARCHAR(20)]
     * The merchant id given by RDP when setting up an account.
     */
    public String getMid() {
        return mid;
    }

    /**
     * @return [CONDITIONAL : success-only][VARCHAR(128)]
     * A SHA-512 signature to proof that this request is coming from the merchant
     */
    public String getSignature() {
        return signature;
    }

    /**
     * @return [conditional : success-only][NUMERIC]
     * UNIX Timestamp - Indicates the time when the payment-page session is destroyed.
     */
    public String getExpiredTimestamp() {
        return expired_timestamp;
    }

    /**
     * @return [CONDITIONAL : success-only][NUMERIC]
     * UNIX Timestamp - Indicates the time when the payment page is created.
     */
    public String getCreatedTimestamp() {
        return created_timestamp;
    }

    /**
     * @return [CONDITIONAL : success-only][VARCHAR(20)]
     * Echo back the order-id sent in the request
     */
    public String getOrderId() {
        return order_id;
    }

    /**
     * @return [CONDITIONAL : success-only][URL]
     * The payment page URL where merchant's system need to redirect to
     */
    public URL getPaymentUrl() {
        return payment_url;
    }

    /**
     * @return [MANDATORY][TEXT]
     * A description on the response_code field
     */
    public String getResponseMsg() {
        return response_msg;
    }

    /**
     * @return [MANDATORY][VARCHAR(10)]
     * Code that indicates the status of a payment.
     * 0 : means successfully create the ticket
     * else : errors happens
     */
    public String getResponseCode() {
        return response_code;
    }

}
