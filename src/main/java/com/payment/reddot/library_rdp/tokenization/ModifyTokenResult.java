/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.payment.reddot.library_rdp.tokenization;

import java.util.Date;

/**
 *
 * @author Red Dot Payment
 */
public class ModifyTokenResult {

    private String ccy;
    private String payerId;
    private String tokenId;
    private String responseCode;
    private String responseMsg;
    private String mid;
    private String orderId;
    private String transactionId;
    private String transactionType;
    private Date createdTimestamp;
    private String acquirerResponseCode;
    private String acquirerResponseMsg;
    private String signature;
    private Date acquirerCreatedTimestamp;
    private String acquirerTransactionId;
    private String acquirerAuthorizationCode;
    private String first6;
    private String last4;
    private String payerName;
    private String expDate;

    public ModifyTokenResult(String bodyResponse) {

    }

    public String getCcy() {
        return ccy;
    }

    public String getPayerId() {
        return payerId;
    }

    public String getTokenId() {
        return tokenId;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public String getResponseMsg() {
        return responseMsg;
    }

    public String getMid() {
        return mid;
    }

    public String getOrderId() {
        return orderId;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public Date getCreatedTimestamp() {
        return createdTimestamp;
    }

    public String getAcquirerResponseCode() {
        return acquirerResponseCode;
    }

    public String getAcquirerResponseMsg() {
        return acquirerResponseMsg;
    }

    public String getSignature() {
        return signature;
    }

    public Date getAcquirerCreatedTimestamp() {
        return acquirerCreatedTimestamp;
    }

    public String getAcquirerTransactionId() {
        return acquirerTransactionId;
    }

    public String getAcquirerAuthorizationCode() {
        return acquirerAuthorizationCode;
    }

    public String getFirst6() {
        return first6;
    }

    public String getLast4() {
        return last4;
    }

    public String getPayerName() {
        return payerName;
    }

    public String getExpDate() {
        return expDate;
    }

}
