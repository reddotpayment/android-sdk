package com.payment.reddot.library_rdp;

import android.content.Intent;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import com.payment.reddot.library_rdp.main.Constant;
import com.payment.reddot.library_rdp.main.DataProcessor;
import com.payment.reddot.library_rdp.main.Verificator;
import com.payment.reddot.library_rdp.payment.CreatePaymentResponse;
import com.payment.reddot.library_rdp.payment.PaymentRequestV2;

import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.ExecutionException;

import io.card.payment.CardIOActivity;
import io.card.payment.CreditCard;

/**
 * Created by Fadli on 4/4/2017.
 */
public class RdpCardIOActivity extends BaseActivity {
    private int request_code;
    private String jsonBeforeSignature, secretKey, mid;
    private String urlRequestPayment;
    private TreeMap<String, String> sortedMap;
    private PaymentRequestV2 request;
    Verificator verificator = new Verificator();
    CreatePaymentResponse createPaymentTicketResponse = null;
    private TextView btnCancel;


    @Override
    protected void setRootView() {
        super.setRootView();
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.layout_webview);
    }

    @Override
    protected void initWidget() {
        super.initWidget();
        btnCancel = (TextView) findViewById(R.id.btnExit);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        Intent scanIntent = new Intent(this, CardIOActivity.class);

        // customize these values to suit your needs.
        scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_EXPIRY, true);
        scanIntent.putExtra(CardIOActivity.EXTRA_SCAN_EXPIRY, true);
        scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_CVV, true);
        scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_CARDHOLDER_NAME, true);

        // MY_SCAN_REQUEST_CODE is arbitrary and is only used within this activity.
        startActivityForResult(scanIntent, Constant.CARD_IO_REQUEST_CODE);
    }

    private String requestTicket(String card_number, String cvv2, String first_name,
                                 String last_name, String expiry_date) {
        Gson gson = new Gson();
        String response = "";
        try {
            urlRequestPayment = getIntent().getExtras().getString("urlRequestPayment");
            jsonBeforeSignature = getIntent().getExtras().getString("jsonBeforeSignature");
            request = gson.fromJson(jsonBeforeSignature, PaymentRequestV2.class);
            secretKey = getIntent().getExtras().getString("secret_key");
            mid = getIntent().getExtras().getString("mid");
            request_code = getIntent().getExtras().getInt("requestCode");
        } catch (Exception e) {

        }

        try {
            sortedMap = new ObjectMapper().readValue(jsonBeforeSignature, TreeMap.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        StringBuilder calculateString = new StringBuilder();
        calculateString.append(mid);
        calculateString.append(request.getOrderId());
        calculateString.append(request.getPaymentType());
        calculateString.append(request.getAmount());
        calculateString.append(request.getCcy());
        calculateString.append(request.getPayerId() == null ? "" : request.getPayerId());
        calculateString.append(secretKey);

        JsonElement jsonElement = gson.toJsonTree(request);
        if (jsonElement.getAsJsonObject().get("payment_channel") == null || jsonElement.getAsJsonObject().get("payment_channel").toString().equals("0")) {
            jsonElement.getAsJsonObject().remove("payment_channel");
        }
        jsonElement.getAsJsonObject().addProperty("card_no", "" + card_number);
        jsonElement.getAsJsonObject().addProperty("cvv2", "" + cvv2);
        jsonElement.getAsJsonObject().addProperty("payer_name", "" + first_name + " " + last_name);
        jsonElement.getAsJsonObject().addProperty("exp_date", "" + expiry_date);
        jsonElement.getAsJsonObject().remove("scan_mode");
        jsonElement.getAsJsonObject().addProperty("signature", verificator.generateHash(calculateString.toString()));
        String jsonAfterSignature = gson.toJson(jsonElement);

        DataProcessor processData = new DataProcessor(jsonAfterSignature, urlRequestPayment);
        try {
            response = processData.execute().get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        return response;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Constant.CARD_IO_REQUEST_CODE) {
            String resultDisplayStr;
            if (data != null && data.hasExtra(CardIOActivity.EXTRA_SCAN_RESULT)) {
                CreditCard scanResult = data.getParcelableExtra(CardIOActivity.EXTRA_SCAN_RESULT);

                //resultDisplayStr = "Card Number: " + scanResult.getFormattedCardNumber() + "\n";
                Gson gson = new Gson();

                String first_name;
                String last_name;

                try {
                    first_name = scanResult.cardholderName.substring(0,
                            scanResult.cardholderName.indexOf(" "));
                    int iSub = scanResult.cardholderName.indexOf(" ") + 1;
                    last_name = scanResult.cardholderName.substring(iSub);
                } catch (Exception e) {
                    first_name = scanResult.cardholderName;
                    last_name = scanResult.cardholderName;
                }

                String expiryMonth = "", expiryYear = "";
                if (scanResult.expiryMonth < 10) {
                    expiryMonth = "0" + scanResult.expiryMonth;
                } else {
                    expiryMonth = "" + scanResult.expiryMonth;
                }

                try {
//                    createPaymentTicketResponse = new CreatePaymentResponse(
//                            requestTicket(scanResult.getFormattedCardNumber().replace(" ", ""),
//                                    scanResult.cvv,
//                                    first_name,
//                                    last_name,
//                                    "" + expiryMonth + scanResult.expiryYear), secretKey);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (getIntent().getExtras().getBoolean("customizable") == true) {
                    if (createPaymentTicketResponse.getResponseCode() == null) {
                        Intent intent = getIntent();
                        setResult(RESULT_CANCELED, intent);
                        intent.putExtra("response", "Cannot get response from Web Service. Try to check your internet connection or contact our support.");
                        finish();
                    } else {
                        Intent intent = getIntent();
                        if (createPaymentTicketResponse.getResponseCode().equals("0")) {
                            setResult(RESULT_OK, intent);
                        } else {
                            setResult(RESULT_CANCELED, intent);
                        }
                        intent.putExtra("response", "" + gson.toJson(createPaymentTicketResponse));
                        finish();
                    }
                } else {
                    if (createPaymentTicketResponse.getResponseCode() == null) {
                        Intent intent = getIntent();
                        setResult(RESULT_CANCELED, intent);
                        intent.putExtra("response", "Cannot get response from Web Service. Try to check your internet connection or contact our support.");
                        finish();
                    } else {
                        if (createPaymentTicketResponse.getResponseCode().equals("0")) {
                            Intent connect = new Intent(RdpCardIOActivity.this, AuthorizationActivity.class);
                            connect.putExtra("payment_url", createPaymentTicketResponse.getPaymentUrl().toString());
                            connect.putExtra("mid", mid);
                            connect.putExtra("secret_key", secretKey);
                            connect.putExtra("environment", getIntent().getExtras().getInt("environment"));
                            connect.putExtra("from_sdk", true);
                            startActivityForResult(connect, request_code);
                        } else {
                            Intent intent = getIntent();
                            if (createPaymentTicketResponse.getResponseCode().equals("0")) {
                                setResult(RESULT_OK, intent);
                            } else {
                                setResult(RESULT_CANCELED, intent);
                            }
                            intent.putExtra("response", "" + gson.toJson(createPaymentTicketResponse));
                            finish();
                        }
                    }
                }
            } else {
                //back button handler
                finish();
            }

            // do something with resultDisplayStr, maybe display it in a textView
            // resultTextView.setText(resultDisplayStr);
        }

        if (requestCode == request_code) {
            if (data != null) {
                Gson gson = new Gson();
                Type stringStringMap = new TypeToken<Map<String, String>>() {
                }.getType();
                Map<String, String> map = gson.fromJson(
                        data.getExtras().getSerializable("response").toString(), stringStringMap);

                Intent intent = getIntent();
                if (map.get("response_code").equals("0")) {
                    setResult(RESULT_OK, intent);
                } else {
                    setResult(RESULT_CANCELED, intent);
                }
                intent.putExtra("response", "" + map.toString());
                finish();
            } else {
                finish();
            }
        }
    }
    // else handle other activity results
}
